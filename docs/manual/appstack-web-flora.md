# 如何使用 Flora 作為新專案的範本

## flora-server

複製下列檔案與目錄：

- mvnw
- mvnw.cmd
- pom.xml
- repo
- src

開啟 pom.xml，並做下列修改：

- 變更 group id：把 io.leanoffice.flora 更換成適當的名稱．
- 變更 artifact id：把 flora-server 更換成適當的名稱．
- 變更 name：把 flora-server 更換成適當的名稱．
- 變更 description：把 Flora Server 更換成適當的名稱．

使用 intelliJ 匯入專案，並做下列修改：

- 變更 package 名稱：把 io.leanoffice.flora 更換成適當的 package 名稱．
- 更名並變更 FloraServer.java
- 更名並變更 FloraConfiguration.java
- 更名並變更 FloraInitializer.java
- 更名並變更 FloraInstaller.java
- 更名並變更 flora-server.properties
- 變更 appstack.properties

## flora-office

複製下列檔案與目錄：

- package.json
- package-lock.json
- pom.xml
- web.xml
- README.md
- .gitignore
- public
- src

開啟 package.json & package-lock.json，並做下列修改：

- 變更專案名稱：flora-office 更換成適當的專案名稱．

開啟 pom.xml，並做下列修改：

-  group id：把 io.leanoffice.flora 更換成適當的名稱．
- 變更 artifact id：把 flora-office 更換成適當的名稱．
- 變更 description：把 Flora Office 更換成適當的名稱．

使用 webstorm 匯入專案，並做下列修改：

- 變更 App.js

