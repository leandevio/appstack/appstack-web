package io.leandev.appstack.web.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleDeserializers;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import com.fasterxml.jackson.datatype.jsr310.ser.ZonedDateTimeSerializer;
import io.leandev.appstack.web.entity.Document;
import io.leandev.appstack.web.json.DocumentDeserializer;
import io.leandev.appstack.json.ZonedDateTimeDeserializer;
import io.leandev.appstack.web.converter.*;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;


/**
 * Spring Repository REST configuration
 */
@Component
public class RepositoryRestConfigurer extends RepositoryRestConfigurerAdapter {
    @Override
    public void configureConversionService(ConfigurableConversionService conversionService) {
        conversionService.addConverter(new StringToDateConverter());
        conversionService.addConverter(new LongToDateConverter());
        conversionService.addConverter(new IntegerToDateConverter());
        conversionService.addConverter(new StringToBytesConverter());
        conversionService.addConverter(new MultipartFileToDocumentConverter());
    }

    /**
     *
     * Set the 'Access-Control-Allow-Origin' header
     *
     * Spring data repository does not recognize the WebMVC's cors setting.
     *
     * We will also add a CORS (Cross-Origin Resource Sharing) filter in our security configuration class. This is
     * needed for the frontend, that is sending requests from the other origin. The CORS filter intercepts requests, and
     * if these are identified as cross origin, it adds proper headers to the request.
     *
     * The following defaults are applied to the CorsRegistration:
     *
     *     Allow all origins.
     *     Allow "simple" methods GET, HEAD and POST.
     *     Allow all headers.
     *     Set max age to 1800 seconds (30 minutes).
     *
     * @param config
     */
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.setDefaultPageSize(10);
        config.setMaxPageSize(200);
//        config.setBasePath("/api");

        config.getCorsRegistry()
                .addMapping("/**")
                .allowedMethods("*")
                .exposedHeaders("ETag", "Exception", "Content-Disposition", "Captcha", "Error")
                .allowedHeaders("*")
                .allowedOrigins("*");


//                .allowedOrigins("http://domain2.com")
//                .allowedMethods("PUT", "DELETE")
//                .allowedHeaders("header1", "header2", "header3")
//                .exposedHeaders("header1", "header2")
//                .allowCredentials(false).maxAge(3600);
    }

    @Override
    public void configureJacksonObjectMapper(ObjectMapper objectMapper) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.setDateFormat(df);

        objectMapper.registerModule(new SimpleModule("MyCustomModule") {
            @Override
            public void setupModule(SetupContext context) {
                SimpleSerializers serializers = new SimpleSerializers();
                SimpleDeserializers deserializers = new SimpleDeserializers();

                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX");
                serializers.addSerializer(ZonedDateTime.class, new ZonedDateTimeSerializer(dateTimeFormatter));

                deserializers.addDeserializer(Document.class, new DocumentDeserializer());
                deserializers.addDeserializer(ZonedDateTime.class, new ZonedDateTimeDeserializer());

                context.addSerializers(serializers);
                context.addDeserializers(deserializers);
            }
        });
    }

}
