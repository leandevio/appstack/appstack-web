package io.leandev.appstack.web.config;

import com.fasterxml.jackson.databind.module.SimpleDeserializers;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import com.fasterxml.jackson.datatype.jsr310.ser.ZonedDateTimeSerializer;
import io.leandev.appstack.json.ZonedDateTimeDeserializer;
import io.leandev.appstack.web.converter.*;
import org.springframework.beans.BeansException;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;


@Configuration
@ComponentScan({"io.leandev.appstack.web.config", "io.leandev.appstack.web.listener", "io.leandev.appstack.web.controller",
        "io.leandev.appstack.web.service", "io.leandev.appstack.web.repository", "io.leandev.appstack.web.converter"})
@EnableJpaRepositories(basePackages = {"io.leandev.appstack.web.repository"})
@EntityScan(basePackages = {"io.leandev.appstack.web.entity"} )
@EnableWebMvc
public class WebConfiguration implements WebMvcConfigurer, ApplicationContextAware {
    private static ApplicationContext applicationContext = null;
    private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
        "classpath:/public/"
    };

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public static Object getBean(String name) {
        return applicationContext.getBean(name);
    }

    public static <T> T getBean(Class<T> type) {
        return applicationContext.getBean(type);
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToDateConverter());
        registry.addConverter(new LongToDateConverter());
        registry.addConverter(new IntegerToDateConverter());
        registry.addConverter(new StringToBytesConverter());
        registry.addConverter(new MultipartFileToDocumentConverter());

    }
    /**
     *
     * Set the 'Access-Control-Allow-Origin' header
     *
     * We will also add a CORS (Cross-Origin Resource Sharing) filter in our security configuration class. This is
     * needed for the frontend, that is sending requests from the other origin. The CORS filter intercepts requests, and
     * if these are identified as cross origin, it adds proper headers to the request.
     *
     * The following defaults are applied to the CorsRegistration:
     *
     *     Allow all origins.
     *     Allow "simple" methods GET, HEAD and POST.
     *     Allow all headers.
     *     Set max age to 1800 seconds (30 minutes).
     *
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods("*")
                .exposedHeaders("ETag", "Exception", "Content-Disposition", "Captcha", "Error")
                .allowedHeaders("*")
                .allowedOrigins("*");
    }

    /**
     *  set the context path by populating the bean factory with a configuration bean.
     */
    @Bean
    public WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> webServerFactoryCustomizer() {
        return factory -> factory.setContextPath("");
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.setDateFormat(df);

        objectMapper.registerModule(new SimpleModule("MyCustomModule") {
            @Override
            public void setupModule(SetupContext context) {
                SimpleSerializers serializers = new SimpleSerializers();
                SimpleDeserializers deserializers = new SimpleDeserializers();

                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX");
                serializers.addSerializer(ZonedDateTime.class, new ZonedDateTimeSerializer(dateTimeFormatter));

                deserializers.addDeserializer(ZonedDateTime.class, new ZonedDateTimeDeserializer());

                context.addSerializers(serializers);
                context.addDeserializers(deserializers);
            }
        });

        return objectMapper;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        registry.addResourceHandler("/public/**")
                .addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS)
                .setCachePeriod(3000);
    }
}
