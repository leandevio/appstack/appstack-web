package io.leandev.appstack.web.config;

import io.leandev.appstack.util.Environment;
import io.leandev.appstack.web.constant.WebSecurityPropsDefault;
import io.leandev.appstack.web.constant.WebSecurityPropsKey;
import io.leandev.appstack.web.filter.RemoteHostFilter;
import io.leandev.appstack.web.handler.CustomAccessDeniedHandler;
import io.leandev.appstack.web.handler.CustomHttp403ForbiddenEntryPoint;
import io.leandev.appstack.web.security.IpFilter;
import io.leandev.appstack.web.service.CustomAuthenticationProvider;
import io.leandev.appstack.web.filter.AuthenticationFilter;
import io.leandev.appstack.web.filter.LoginFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import java.util.Arrays;
import java.util.Optional;

/**
 * The @CacheConfiguration.java and @EnableWebSecurity annotations switch off the default web security configuration and we can
 * define our own configuration in this class. Inside the configure(HttpSecurity http) method, we can define which
 * endpoints in our application are secured and which are not.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
    private static Environment environment = Environment.getDefaultInstance();
    private static boolean SECURITYENABLED = environment.propertyAsBoolean(WebSecurityPropsKey.SECURITYENABLED, WebSecurityPropsDefault.SECURITYENABLED);
    private static String SECURITYLOGINPATH = environment.property(WebSecurityPropsKey.SECURITYLOGINPATH, WebSecurityPropsDefault.SECURITYLOGINPATH);
    private static String SECURITYPERMIT = environment.property(WebSecurityPropsKey.SECURITYPERMIT, "");
    private static String SECURITYDENY = environment.property(WebSecurityPropsKey.SECURITYDENY, "");
    private static final String SECURITYERRORPATH = "/error";

    @Autowired
    private CustomAuthenticationProvider authProvider;
    private final Optional<IpFilter> ipFilter;
    private static ApplicationContext context;

    public static <T> Optional<T> getBean(Class<T> clazz) {
        return context == null ? Optional.empty() : Optional.ofNullable(context.getBean(clazz));
    }

    public WebSecurityConfiguration(Optional<IpFilter> ipFilter, ApplicationContext applicationContext) {
        this.ipFilter = ipFilter;
        context = applicationContext;
    }

    public RemoteHostFilter remoteHostFilter(IpFilter ipFilter) {
        RemoteHostFilter filter = new RemoteHostFilter(ipFilter);
        return filter;
    }

    public LoginFilter loginFilter(AuthenticationManager authenticationManager) {
        LoginFilter filter = new LoginFilter(SECURITYLOGINPATH, authenticationManager);
        return filter;
    }

    public AuthenticationFilter authenticationFilter() {
        AuthenticationFilter filter = new AuthenticationFilter();
        return filter;
    }
    /**
     * Add a new configureGlobal method to enable users from the database. we are using the BCrypt algorithm. This can
     * be easily implemented with the Spring Security BCryptPasswordEncoder class. Now, the password must be hashed
     * using BCrypt before it's saved to the database.
     *
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authProvider);
    }

    /**
     *
     * Inside the configure(HttpSecurity http) method, we can define which endpoints in our application are secured and which are not.
     *
     * we define that the POST method request to the /login endpoint is allowed without authentication and that requests
     * to all other endpoints need authentication. We also define the filters to be used in the /login and other
     * endpoints by using the addFilterBefore method
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Disable the csrf protection provided by Spring Security because we are going to use the JWT to defense again
        // the csrf attack.
        http.cors();

        // X-Content-Type-Options nosniff
        http.csrf().disable();

        // Content-Security-Policy script-src 'self'
        // 'unsafe-inline' is required for h2 console
        http.headers().contentSecurityPolicy("script-src 'self'  'unsafe-inline'");

        // h2 console
        // X-Frame-Options SAMEORIGIN
        http.headers().frameOptions().sameOrigin();

        configSessionManagement(http);

        http.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(loginFilter(authenticationManager()), AuthenticationFilter.class);

        if(ipFilter.isPresent()) {
            http.addFilterBefore(remoteHostFilter(ipFilter.get()), LoginFilter.class);
        }

        if(SECURITYENABLED) {
            http.authorizeRequests(authorize -> {
                        String[] permittedPatterns = (SECURITYPERMIT == "") ? new String[0] : SECURITYPERMIT.split(",");
                        String[] deniedPatterns = (SECURITYDENY == "") ? new String[0] : SECURITYDENY.split(",");
                        authorize.antMatchers(permittedPatterns).permitAll();
                        authorize.antMatchers(deniedPatterns).denyAll();
                    })
                    .authorizeRequests()
                    .antMatchers("/").permitAll()
                    .antMatchers(HttpMethod.POST, SECURITYLOGINPATH).permitAll()
                    .antMatchers(HttpMethod.GET, SECURITYERRORPATH).permitAll()
                    .anyRequest().authenticated()
                    .and()
                    .exceptionHandling().accessDeniedHandler(new CustomAccessDeniedHandler())
                    .and()
                    .exceptionHandling().authenticationEntryPoint(new CustomHttp403ForbiddenEntryPoint());
        } else {
            http.authorizeRequests()
                    .anyRequest().permitAll();
        }
    }

    /**
     * Disable the session creation make the application stateless.
     *
     * Spring Security session management policy:
     * - always – A session will always be created if one doesn't already exist.
     * - ifRequired – A session will be created only if required (default).
     * - never – The framework will never create a session itself, but it will use one if it already exists.
     * - stateless – No session will be created or used by Spring Security.
     *
     * SessionCreationPolicy.STATELESS is the strictest session creation option to guarantee that the application won't create any session at all.
     *
     * @param http
     * @throws Exception
     */
    private void configSessionManagement(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    /**
     * We will also add a CORS (Cross-Origin Resource Sharing) filter in our security configuration class. This is
     * needed for the frontend, that is sending requests from the other origin. The CORS filter intercepts requests, and
     * if these are identified as cross origin, it adds proper headers to the request. For that, we will use Spring
     * Security's CorsConfigurationSource interface. In this example, we will allow all HTTP methods and headers. You
     * can define the list of allowed origins, methods, and headers here, if you need more finely graded definition.
     *
     * @return
     */
    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowedOrigins(Arrays.asList("*"));
        config.setAllowedMethods(Arrays.asList("*"));
        config.setAllowedHeaders(Arrays.asList("*"));
        config.setExposedHeaders(Arrays.asList("ETag", "Exception", "Content-Disposition", "Captcha", "Error"));
        config.setAllowCredentials(true);
        config.applyPermitDefaultValues();
        source.registerCorsConfiguration("/**", config);
        return source;
    }
}