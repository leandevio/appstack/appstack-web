package io.leandev.appstack.web.constant;

import io.leandev.appstack.security.constant.SecurityPropsDefault;

public class WebSecurityPropsDefault extends SecurityPropsDefault {
    public static final String SECURITYLOGINPATH = "/login";
}
