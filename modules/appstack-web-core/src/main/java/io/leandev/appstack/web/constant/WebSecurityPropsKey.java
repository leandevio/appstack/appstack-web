package io.leandev.appstack.web.constant;

import io.leandev.appstack.security.constant.SecurityPropsKey;

public class WebSecurityPropsKey extends SecurityPropsKey {
    public static final String SECURITYLOGINPATH = "security.login.path";
    public static final String SECURITYPERMIT = "security.permit";
    public static final String SECURITYDENY = "security.deny";
}
