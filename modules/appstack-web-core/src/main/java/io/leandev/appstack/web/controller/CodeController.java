package io.leandev.appstack.web.controller;

import io.leandev.appstack.exception.NotFoundException;
import io.leandev.appstack.search.Predicate;
import io.leandev.appstack.util.Item;
import io.leandev.appstack.web.entity.Code;
import io.leandev.appstack.web.service.CodeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("codes")
public class CodeController {

    private final CodeService codeService;

    public CodeController(CodeService codeService) {
        this.codeService = codeService;
    }

    /**
     * Find all code data by search criteria and page request.
     *
     * @param predicate
     * @param pageable
     * @return
     */
    @RequestMapping(method= RequestMethod.GET, produces =  "application/json;charset=UTF-8")
    public ResponseEntity<?> findAll(
            @RequestParam(required = false) Predicate predicate,
            Pageable pageable) {
        Page<Code> page = codeService.findAll(predicate, pageable);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("TotalElements", String.valueOf(page.getTotalElements()));

        return ResponseEntity.ok(page.getContent());
    }

    /**
     * Find all code data by type, search criteria and page request.
     *
     * @param type
     * @return
     */
    @RequestMapping(value="/{type}" ,method= RequestMethod.GET, produces =  "application/json;charset=UTF-8")
    public ResponseEntity<?> findAllByType(
            @PathVariable String type) {

        List<Code> codes = StreamSupport.stream(codeService.findAllByType(type).spliterator(), false)
                .collect(Collectors.toList());

        return ResponseEntity.ok(codes);
    }

    /**
     * Find the code data by type & code
     *
     * @param type
     * @param value
     * @return
     */
    @RequestMapping(value = "/{type}/{value}" ,method=RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public ResponseEntity<?> findByTypeAndCode(@PathVariable String type,
                                               @PathVariable String value) {
        Optional<Code> code = codeService.findByTypeAndValue(type, value);
        code.orElseThrow(() -> new NotFoundException(String.format("Code data not found. Type = '%s',  Value = '%s'.", type, value)));

        return ResponseEntity.ok(code.get());
    }

    /**
     * Create a code
     *
     * @param type
     * @return
     */
    @RequestMapping(value = "/{type}" ,method=RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<?> post(@PathVariable String type, @RequestBody Map<String, Object> props) throws URISyntaxException {
        Code code = new Code();
        Item<Code> item = new Item<>(code);
        item.copyProperties(props, "type", "uuid", "version");
        code.setType(type);
        code = codeService.save(code);

        return ResponseEntity.status(HttpStatus.CREATED).body(code);
    }

    @RequestMapping(value = "/{type}/{value}", method = {RequestMethod.PATCH, RequestMethod.PUT, RequestMethod.POST},
            produces = "application/json;charset=UTF-8")
    public @ResponseBody ResponseEntity<?> patch(
            @PathVariable String type, @PathVariable String value, @RequestBody Map<String, Object> props) {

        Optional<Code> data = codeService.findByTypeAndValue(type, value);

        data.orElseThrow(() -> new NotFoundException(String.format("Code data not found. Type = '%s',  Value = '%s'.", type, value)));

        Code code = data.get();

        Item<Code> item = new Item<>(code);

        item.copyProperties(props, "type", "value", "code", "uuid", "version");

        code = codeService.save(item.getBean());

        return ResponseEntity.ok(code);
    }

    /**
     * Delete a code
     *
     * @param type
     * @param value
     */
    @RequestMapping(value="/{type}/{value}" ,method=RequestMethod.DELETE,produces = "application/hal+json;charset=UTF-8" )
    @ResponseStatus(code= HttpStatus.NO_CONTENT)
    public void erase(@PathVariable String type,
                      @PathVariable String value) {
        codeService.delete(type, value);
        return;
    }

}
