package io.leandev.appstack.web.controller;

import io.leandev.appstack.web.entity.Document;
import io.leandev.appstack.web.service.DocumentService;
import io.leandev.appstack.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@RestController
@RequestMapping("documents")
public class DocumentController {

    private final DocumentService service;

    @Autowired
    public DocumentController(DocumentService service) {
        this.service = service;
    }

//    @RequestMapping(method = RequestMethod.GET, value = "/{uuid}")
//    public @ResponseBody
//    ResponseEntity<?> get(@PathVariable String uuid) throws UnsupportedEncodingException {
//        Optional<Document> data = service.findById(uuid);
//        data.orElseThrow(NotFoundException::new);
//        if(!data.isPresent()) {
//            return ResponseEntity.notFound().build();
//        }
//
//        Document document = data.get();
//
//        return ResponseEntity.ok(toResource(document));
//    }

    @RequestMapping(method = RequestMethod.GET, value = "/{uuid}")
    public ResponseEntity<byte[]> get(@PathVariable String uuid) throws UnsupportedEncodingException {
        Optional<Document> data = service.findById(uuid);

        data.orElseThrow(NotFoundException::new);
        if(!data.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Document document = data.get();
        HttpHeaders headers = new HttpHeaders();

        MediaType mediaType;
        if(document.getType()==null) {
            mediaType = MediaType.APPLICATION_OCTET_STREAM;
        } else {
            String[] tokens =(document.getType().split("/"));
            mediaType = new MediaType(tokens[0], tokens[1]);
        }
        headers.setContentType(mediaType);
        headers.setContentLength(document.getSize().longValue() );
        String filename = URLEncoder.encode(document.getName(), "UTF-8").replace("+","%20");
        String contentDisposition = "attachment; filename*=UTF-8''" + filename;
        headers.add("Content-Disposition", contentDisposition);
        headers.setETag("\"" + document.getVersion().toString() + "\"");
        headers.setLastModified(document.getModifiedOn().getTime());
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());

        return new ResponseEntity<>(document.getBytes(), headers, HttpStatus.OK);

    }

    private EntityModel<Document> toResource(Document document) throws UnsupportedEncodingException {
        Link self = linkTo(methodOn(DocumentController.class).get(document.getUuid())).withSelfRel();
//        Link download = linkTo(methodOn(DocumentController.class).download(document.getUuid())).withRel("download");
        EntityModel<Document> resource = new EntityModel<>(document, self);
        return resource;
    }

}
