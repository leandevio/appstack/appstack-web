package io.leandev.appstack.web.controller;

import cz.jirutka.rsql.parser.RSQLParser;
import io.leandev.appstack.web.entity.Code;
import io.leandev.appstack.data.rsql.RsqlSpecificationVisitor;
import io.leandev.appstack.web.service.CodeService;
import io.leandev.appstack.exception.NotFoundException;
import io.leandev.appstack.util.Item;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.*;

import org.springframework.hateoas.server.LinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping("code")
public class HalCodeController {
    private final CodeService service;
    private final RSQLParser rsqlParser = new RSQLParser();

    public HalCodeController(CodeService service) {
        this.service = service;
    }

    /**
     * Find all code data by search criteria and page request.
     *
     * @param predicate
     * @param pageable
     * @param assembler
     * @return
     */
    @RequestMapping(method= RequestMethod.GET, produces =  "application/hal+json;charset=UTF-8")
    public ResponseEntity<?> findAll(
            @RequestParam(required = false) String predicate, Pageable pageable,
            PagedResourcesAssembler assembler) {
        if(pageable.getSort().getOrderFor("priority")==null) {
            pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort().and(Sort.by("priority").descending()));
        }
        Specification<Code> spec = (predicate==null) ? null : rsqlParser.parse(predicate).accept(new RsqlSpecificationVisitor<>());
        Page<Code> page = service.findAll(spec, pageable);

        PagedModel resources = assembler.toModel(page, item -> toResource((Code) item));

        return ResponseEntity.ok(resources);
    }

    /**
     * Find all code data by type, search criteria and page request.
     *
     * @param type
     * @return
     */
    @RequestMapping(value="/{type}" ,method= RequestMethod.GET, produces =  "application/hal+json;charset=UTF-8")
    public ResponseEntity<?> findAllByType(
            @PathVariable String type,
            @RequestParam(required = false) String predicate, Pageable pageable,
            @RequestParam(required = false) Integer size,
            PagedResourcesAssembler assembler) {

        CollectionModel<Code> resources;
        if(predicate==null&&size!=null&&size<=0) {
            Iterable<Code> iterable = service.findAllByType(type);
            List<Code> codes = StreamSupport
                    .stream(iterable.spliterator(), false)
                    .collect(Collectors.toList());
            resources =  new CollectionModel<>(codes);
        } else {
            Specification<Code> spec = Specification.where((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("type"), type));
            if(predicate!=null) {
                spec = spec.and(rsqlParser.parse(predicate).accept(new RsqlSpecificationVisitor<>()));
            }
            if(pageable.getSort().getOrderFor("priority")==null) {
                pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort().and(Sort.by("priority").descending()));
            }
            Page<Code> page = service.findAll(spec, pageable);
            resources = assembler.toModel(page, item -> toResource((Code) item));
        }

        return ResponseEntity.ok(resources);
    }

    /**
     * Find the code data by type & code
     *
     * @param type
     * @param code
     * @return
     */
    @RequestMapping(value = "/{type}/{code}" ,method=RequestMethod.GET, produces = "application/hal+json;charset=UTF-8")
    public ResponseEntity<?> findByTypeAndCode(@PathVariable String type,
                                    @PathVariable String code) {
        Optional<Code> data = service.findByTypeAndCode(type, code);
        data.orElseThrow(NotFoundException::new);

        Code bean = data.get();
        EntityModel<Code> resource = toResource(bean);

        return ResponseEntity.ok(resource);
    }

    /**
     * Create a code
     *
     * @param type
     * @return
     */
    @RequestMapping(value = "/{type}" ,method=RequestMethod.POST, produces = "application/hal+json;charset=UTF-8")
    public ResponseEntity<?> post(@PathVariable String type, @RequestBody Map<String, Object> props) throws URISyntaxException {
        Code bean = new Code();
        Item<Code> item = new Item<>(bean);
        item.copyProperties(props, "type", "uuid", "version");
        bean.setType(type);
        bean = service.save(bean);
        EntityModel<Code> resource = toResource(bean);

        return ResponseEntity.created(new URI(resource.getLink(IanaLinkRelations.SELF).get().expand().getHref())).body(resource);
    }

    @RequestMapping(value = "/{type}/{code}", method = {RequestMethod.PATCH, RequestMethod.PUT, RequestMethod.POST},
            produces = "application/hal+json;charset=UTF-8")
    public @ResponseBody ResponseEntity<?> patch(
            @PathVariable String type, @PathVariable String code, @RequestBody Map<String, Object> props) {

        Optional<Code> data = service.findByTypeAndCode(type, code);

        data.orElseThrow(NotFoundException::new);

        Code bean = data.get();

        Item<Code> item = new Item<>(bean);

        item.copyProperties(props, "type", "code", "uuid", "version");

        bean = service.save(item.getBean());

        EntityModel<Code> resource = toResource(bean);

        return ResponseEntity.ok(resource);
    }

    /**
     * Delete a code
     *
     * @param type
     * @param code
     */
    @RequestMapping(value="/{type}/{code}" ,method=RequestMethod.DELETE,produces = "application/hal+json;charset=UTF-8" )
    @ResponseStatus(code= HttpStatus.NO_CONTENT)
    public void erase(@PathVariable String type,
                      @PathVariable String code) {
        service.delete(type, code);
        return;
    }

    private EntityModel<Code> toResource(Code code) {
        LinkBuilder builder = linkTo(HalCodeController.class).slash(code.getType()).slash(code.getCode());
        Link self = builder.withSelfRel();
        EntityModel<Code> resource = new EntityModel<>(code, self);

        return resource;
    }

}
