package io.leandev.appstack.web.converter;

import io.leandev.appstack.converter.Converter;
import io.leandev.appstack.web.entity.Document;
import io.leandev.appstack.exception.ConversionException;
import io.leandev.appstack.util.Item;

import java.util.Map;

public class DocumentConverter implements Converter<Document> {
    @Override
    public Document convert(Object value) {
        if(value==null) return null;

        if(!(value instanceof Map)) {
            throw new ConversionException("Only support convert Map to Document.");
        }

        Document document = new Document();
        Item item = new Item(document);
        item.copyProperties(value);
        return document;
    }
}
