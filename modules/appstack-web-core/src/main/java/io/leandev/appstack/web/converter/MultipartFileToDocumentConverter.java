package io.leandev.appstack.web.converter;

import io.leandev.appstack.web.entity.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public class MultipartFileToDocumentConverter implements Converter<MultipartFile, Document> {

    @Override
    public Document convert(MultipartFile multipartFile) {
        Document document = new Document();
        document.setName(multipartFile.getOriginalFilename());
        document.setType(multipartFile.getContentType());
        try {
            document.setBytes(multipartFile.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        document.setSize(multipartFile.getSize());

        return document;
    }
}
