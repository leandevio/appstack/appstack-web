package io.leandev.appstack.web.converter;

import io.leandev.appstack.converter.BytesConverter;
import io.leandev.appstack.converter.DateConverter;
import org.springframework.core.convert.converter.Converter;

import java.util.Date;

public class StringToBytesConverter implements Converter<String, byte[]> {
    private BytesConverter converter = new BytesConverter();
    @Override
    public byte[] convert(String source) {
        return converter.convert(source);
    }
}
