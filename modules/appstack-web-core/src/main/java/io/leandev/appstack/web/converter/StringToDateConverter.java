package io.leandev.appstack.web.converter;

import io.leandev.appstack.converter.DateConverter;
import org.springframework.core.convert.converter.Converter;
import java.util.Date;

public class StringToDateConverter implements Converter<String, Date> {
    private DateConverter converter = new DateConverter();
    @Override
    public Date convert(String source) {
        return converter.convert(source);
    }
}
