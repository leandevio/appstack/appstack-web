package io.leandev.appstack.web.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="APS_Code", uniqueConstraints=@UniqueConstraint(columnNames={"type", "code"}))
public class Code {
    /** 版本 */
    private Integer version;
    /** 識別 */
    private String uuid;
    /** 分類 */
    private String type;
    /** 代碼 */
    private String code;
    /** 文字 */
    private String text;
    /** 圖示 */
    private String icon;
    /** 順序 */
    private Double priority = 0D;
    /** 資料來源 */
    private String source = "user";
    /** 停用 */
    private Boolean disabled = false;
    /** 鎖定 */
    private Boolean locked = false;
    /** 不建議使用 */
    private Boolean deprecated = false;

    /**
     * Getter: {@link #version}.
     * @return {@link #version}.
     */
    @Version
    public Integer getVersion() {
        return version;
    }

    /**
     * Setter: {@link #version}.
     * @param version {@link #version}.
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * Getter: {@link #uuid}.
     * @return {@link #uuid}.
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length=36)
    @Size(max = 36)
    public String getUuid() {
        return uuid;
    }

    /**
     * Setter: {@link #uuid}.
     * @param uuid {@link #uuid}.
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Getter: {@link #type}.
     * @return {@link #type}.
     */
    @Column(length = 100, nullable = false)
    @NotNull
    @Size(max = 100)
    public String getType() {
        return type;
    }

    /**
     * Setter: {@link #type}.
     * @param type {@link #type}.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Getter: {@link #code}.
     * @return {@link #code}.
     */
    @Column(length = 100, nullable = false)
    @NotNull
    @Size(max = 100)
    public String getCode() {
        return code;
    }

    /**
     * Setter: {@link #code}.
     * @param code {@link #code}.
     */
    public void setCode(String code) {
        this.code = code;
    }


    @Deprecated
    @Transient
    public String getValue() {
        return this.code;
    }

    @Deprecated
    @Transient
    public void setValue(String code) {
        this.code = code;
    }


    /**
     * Getter: {@link #text}.
     * @return {@link #text}.
     */
    @Column(length = 2000)
    @Size(max = 2000)
    public String getText() {
        return text;
    }

    /**
     * Setter: {@link #text}.
     * @param text {@link #text}.
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Getter: {@link #icon}.
     * @return {@link #icon}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getIcon() {
        return icon;
    }

    /**
     * Setter: {@link #icon}.
     * @param icon {@link #icon}.
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }


    /**
     * Getter: {@link #priority}.
     * @return {@link #priority}.
     */
    @Column(nullable = false)
    @NotNull
    public double getPriority() {
        return priority;
    }

    /**
     * Setter: {@link #priority}.
     * @param priority {@link #priority}.
     */
    public void setPriority(double priority) {
        this.priority = priority;
    }

    /**
     * Getter: {@link #source}.
     * @return {@link #source}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getSource() {
        return source;
    }

    /**
     * Setter: {@link #source}.
     * @param source {@link #source}.
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * Getter: {@link #disabled}.
     * @return {@link #disabled}.
     */
    public Boolean isDisabled() {
        return disabled;
    }

    /**
     * Setter: {@link #disabled}.
     * @param disabled {@link #disabled}.
     */
    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    /**
     * Getter: {@link #locked}.
     * @return {@link #locked}.
     */
    public Boolean isLocked() {
        return locked;
    }

    /**
     * Setter: {@link #locked}.
     * @param locked {@link #locked}.
     */
    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    /**
     * Getter: {@link #deprecated}.
     * @return {@link #deprecated}.
     */
    public Boolean isDeprecated() {
        return deprecated;
    }

    /**
     * Setter: {@link #deprecated}.
     * @param deprecated {@link #deprecated}.
     */
    public void setDeprecated(Boolean deprecated) {
        this.deprecated = deprecated;
    }
}
