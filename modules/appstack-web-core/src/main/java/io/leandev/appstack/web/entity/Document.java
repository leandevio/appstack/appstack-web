package io.leandev.appstack.web.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.net.URI;
import java.util.*;

@Entity
@Table(name="APS_Document")
public class Document {
//
//            '.txt': 'text/plain',
//                    '.csv': 'text/csv',
//                    '.pdf': 'application/pdf',
//                    '.xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
//                    '.doc': 'application/msword',
//                    '.docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
//                    '.ppt': 'application/vnd.ms-powerpoint',
//                    '.pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
//                    '.zip': 'application/zip',
//                    '.json': 'application/json'
//    private static final Logger LOGGER = LogManager.getLogger(Document.class);
//    private static final Path docFolder = Environment.getDefaultInstance().docFolder();
//    private static final String docStore = Environment.getDefaultInstance().property("doc.store", "database");
//    private static final boolean isAtomic = docStore.equalsIgnoreCase("database");
    private static String DEFAULTMEDIATYPE = "application/octet-stream";
    /** 版本 */
    private Integer version;
    /** 識別 */
    private String uuid;
    /** 編號 */
    private String uri;
    /** 名稱 */
    private String name;
    /** 標題 */
    private String title;
    /** 說明 */
    private String description;
    /** Media Type */
    private String type = DEFAULTMEDIATYPE;
    /** Byte Data */
    private byte[] bytes;
    /** 大小 */
    private Long size;
    /** 建立者 */
    private String preparedBy;
    /** 修改者 */
    private String modifiedBy;
    /** 建立日期 */
    private Date createdOn = new Date();
    /** 修改日期 */
    private Date modifiedOn = new Date();
    /** 分類 */
    private String category;
    /** 標籤 */
    private Set<String> tags = new HashSet<>();
    /** 自訂屬性 */
    private Map<String, String> attributes = new HashMap<>();

    private URI href;

    /**
     * Getter: {@link #version}.
     * @return {@link #version}.
     */
    @Version
    public Integer getVersion() {
        return version;
    }

    /**
     * Setter: {@link #version}.
     * @param version {@link #version}.
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * Getter: {@link #uuid}.
     * @return {@link #uuid}.
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length = 36)
    @Size(max = 36)
    public String getUuid() {
        return uuid;
    }

    /**
     * Setter: {@link #uuid}.
     * @param uuid {@link #uuid}.
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Getter: {@link #uri}.
     * @return {@link #uri}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getUri() {
        return uri;
    }

    /**
     * Setter: {@link #uri}.
     * @param uri {@link #uri}.
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     * Getter: {@link #name}.
     * @return {@link #name}.
     */
    @Column(length = 100, nullable = false)
    @NotNull
    @Size(max = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter: {@link #title}.
     * @return {@link #title}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter: {@link #type}.
     * @return {@link #type}.
     */
    @Column(length = 100)
    @Size(max = 100)
    @NotNull
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * Getter: {@link #size}.
     * @return {@link #size}.
     */
    @Column(name = "\"size\"")
    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    /**
     * Getter: {@link #createdOn}.
     * @return {@link #createdOn}.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column
    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }


    /**
     * Getter: {@link #modifiedOn}.
     * @return {@link #modifiedOn}.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column
    @LastModifiedDate
    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    /**
     * Getter: {@link #description}.
     * @return {@link #description}.
     */
    @Column(length = 2000)
    @Size(max = 2000)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter: {@link #preparedBy}.
     * @return {@link #preparedBy}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getPreparedBy() {
        return preparedBy;
    }

    public void setPreparedBy(String preparedBy) {
        this.preparedBy = preparedBy;
    }

    /**
     * Getter: {@link #modifiedBy}.
     * @return {@link #modifiedBy}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }


    /**
     * Getter: {@link #category}.
     * @return {@link #category}.
     */
    @Column(length = 100)
    @Size(max = 100)
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Getter: {@link #tags}.
     * @return {@link #tags}.
     */
    @ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(name = "APD_Document_Tags", joinColumns = @JoinColumn(name = "Owner"))
    @Column(length = 100)
    @Size(max = 100)
    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public boolean addTag(String tag) {
        return this.tags.add(tag);
    }

    public boolean removeTag(String tag) {
        return this.tags.remove(tag);
    }

    /**
     * Getter: {@link #attributes}.
     * @return {@link #attributes}.
     */
    @ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(name="APS_Document_Attributes",  joinColumns = @JoinColumn(name = "Owner"))
    @MapKeyColumn(length=100)
    @Column(length = 200)
    @Size(max = 200)
    public Map<String, String> getAttributes() {
        return this.attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    @Transient
    public String getAttribute(String name) {
        return this.attributes.get(name);
    }

    public String setAttribute(String name, String value) {
        return this.attributes.put(name, value);
    }

    /**
     * Getter: {@link #bytes}.
     * @return {@link #bytes}.
     */
    @JsonIgnore
    @Lob
    public byte[] getBytes() {
        return this.bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    @Transient
    public URI getHref() {
        return this.href;
    }

    public void setHref(URI href) {
        this.href = href;
    }

    public String toString() {
        return this.name;
    }

//    @PrePersist
//    public void persistBlob() {
//        if(isAtomic) return;
//
//        if(bytes==null||bytes.length==0) return;
//
//        if(uri==null) uri = generateUri();
//        Path blob = resolvePath();
//
//        try {
//            Files.deleteIfExists(blob);
//        } catch (IOException e) {
//            LOGGER.warn(e);
//        }
//
//        createDir(blob.getParent());
//
//        try {
//            Files.write(blob, bytes);
//        } catch (IOException e) {
//            LOGGER.warn(e);
//        }
//        bytes = new byte[0];
//    }
//
//    @PostRemove
//    public void removeBlob() {
//        if(isAtomic) return;
//
//        if(uri==null) return;
//
//        Path blob = resolvePath();
//
//        // compatible
//        if(!Files.exists(blob)) blob = resolvePath2();
//
//        try {
//            Files.deleteIfExists(blob);
//        } catch (IOException e) {
//            LOGGER.warn(e);
//        }
//    }
//
//    @PostLoad
//    public void loadBlob() {
//        if(isAtomic) return;
//
//        if(bytes!=null&&bytes.length>0) return;
//
//        if(uri==null) return;
//
//        Path blob = resolvePath();
//
//        // compatible
//        if(!Files.exists(blob)) blob = resolvePath2();
//
//        if(!Files.exists(blob)) return;
//
//        try {
//            bytes = Files.readAllBytes(blob);
//        } catch (IOException e) {
//            throw new org.uweaver.core.exception.IOException(e);
//        }
//    }
//
//    private void createDir(Path dir) {
//        try {
//            Files.createDirectories(dir);
//        } catch (IOException e) {
//            throw new org.uweaver.core.exception.IOException(e);
//        }
//    }
//
//    private Path resolvePath() {
//        if(this.uri==null) return null;
//        Path path = docFolder.resolve(this.uri);
//        return path;
//    }
//
//    private Path resolvePath2() {
//        Path path = docFolder.resolve(uri.substring(0, 2)).resolve(uri.substring(2, 4)).resolve(uri + ".dat");
//        return path;
//    }
//
//    private String generateUri() {
//        String uuid = UUID.randomUUID().toString();
//        Path uri = Paths.get(uuid.substring(0, 8)).resolve(uuid.substring(9, 13)).resolve(uuid.substring(14) + "." + resolveExtension());
//        return uri.toString();
//    }
//
//    private String resolveExtension() {
//        String extension = "bin";
//        String filename = this.getName();
//        if(filename!=null) {
//            int index = filename.lastIndexOf(".");
//            if(index!=-1 && filename.length()>index) {
//                extension = filename.substring(index+1);
//            }
//        }
//        return extension;
//    }
}
