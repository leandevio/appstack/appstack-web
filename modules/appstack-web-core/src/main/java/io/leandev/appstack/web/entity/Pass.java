package io.leandev.appstack.web.entity;

import io.leandev.appstack.bean.Model;
import io.leandev.appstack.json.JsonParser;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import io.leandev.appstack.web.service.PassListener;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="APS_Pass",uniqueConstraints=@UniqueConstraint(columnNames={"principal"}))
@EntityListeners(PassListener.class)
public class Pass implements Serializable {
    private static final long serialVersionUID = 1L;
    private static JsonParser jsonParser = new JsonParser();
    /** 識別 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length=36)
    private String uuid;

    /** 版本 */
    @Version
    private Integer version;

    /** 用戶名稱 */
    @Column(nullable = false)
    @NotNull
    private String principal;

    /** 密碼 */
    @Column
    private String credentials;

    /**
     * 限定的 IP 名單
     * 只有在 IP 名單內的電腦才能使用這個帳號。
     * 可以使用 "," 分隔來建立多個 IP 限制。IP 限制地表示法可以是 ip address 或是 CIDR。
     */
    @Column
    private String ipRestrictions;

    /** Privilege.uuid */
    @Singular
    @ElementCollection(fetch = FetchType.EAGER)
    @Column
    private Set<String> privileges = new HashSet<>();

    /** Permission.uuid */
    @Singular
    @ElementCollection(fetch = FetchType.EAGER)
    @Column
    private Set<String> authorities = new HashSet<>();

    @Transient
    public boolean getEnabled() {
        Date now = new Date();
        Date activationDate = this.getActivationDate();
        return (activationDate != null && activationDate.before(now));
    }

    @Transient
    public boolean getAccountNonExpired() {
        Date now = new Date();
        Date expiryDate = this.getExpiryDate();
        return (expiryDate == null || expiryDate.after(now));
    }

    @Transient
    public boolean getCredentialsNonExpired() {
        return true;
    }

    @Transient
    public boolean getAccountNonLocked() {
        Date now = new Date();
        Date suspensionDate = this.getSuspensionDate();
        return (suspensionDate == null || suspensionDate.after(now));

    }

    /** 顯示名稱 */
    @Column(length = 1000)
    private String name;

    /** 電子郵件 */
    @Column(length = 100)
    private String email;

    /** 行動電話*/
    @Column(length = 100)
    private String mobile;

    /** 啟用日期 */
    @Column(length = 100)
    @Temporal(TemporalType.TIMESTAMP)
    private Date activationDate;

    /** 失效日期 */
    @Column(length = 100)
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiryDate;

    /** 停權日期 */
    @Column(length = 100)
    @Temporal(TemporalType.TIMESTAMP)
    private Date suspensionDate;

    /** 備註 */
    @Column(length = 2000)
    private String note;

    /** 建立日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date createdOn;

    /** 建立者 */
    @Column(length = 100)
    private String createdBy;

    /** 修改日期 */
    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date modifiedOn;

    /** 修改者 */
    @Column(length = 100)
    private String modifiedBy;

    /** 區域 */
    @Column
    private String site;

    /** 分類 */
    @Column
    private String category;

    /** 其他資料 */
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    @Column
    private String properties;

    @Transient
    private Model _properties;

    public void _setProperties(String json) throws IOException {
        this._properties = jsonParser.readValue(json, Model.class);
    }

    private Model _getProperties() {
        if(this._properties == null) {
            this._properties = jsonParser.readValue(properties, Model.class);
        }
        return this._properties;
    }


    @Transient
    public String getPropertyAsString(String name) {
        return this._getProperties().getAsString(name);
    }

    @Transient
    public void setProperty(String name, Object value) {
        Model properties = this._getProperties();
        properties.put(name, value);
        this.properties = jsonParser.writeValueAsString(properties);
    }

    @Transient
    public Integer getPropertyAsInteger(String name) {
        return this._getProperties().getAsInteger(name);
    }

    @Transient
    public Boolean getPropertyAsBoolean(String name) {
        return this._getProperties().getAsBoolean(name);
    }

    @Transient
    public Date getPropertyAsDate(String name) {
        return this._getProperties().getAsDate(name);
    }

    @Transient
    public boolean isActivated() {
        Date now = new Date();
        return (this.activationDate!=null && this.activationDate.compareTo(now)<=0);
    }

    @Transient
    public boolean isExpired() {
        Date now = new Date();
        return (this.expiryDate!=null && this.expiryDate.compareTo(now)<=0);
    }

    @Transient
    public boolean isSuspended() {
        Date now = new Date();
        return (this.suspensionDate!=null && this.suspensionDate.compareTo(now)<=0);
    }

}
