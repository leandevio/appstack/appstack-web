package io.leandev.appstack.web.entity;

import io.leandev.appstack.web.service.PermissionListener;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EntityListeners(PermissionListener.class)
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
/**
 * A permission is a property of an object, such as a file. It says which agents are permitted to use the object, and what they are permitted to do (read it, modify it, etc.)
 */
public class Permission {
    /** 識別 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length=36)
    @Size(max = 36)
    private String uuid;

    /** 版本 */
    @Version
    private Integer version;

    /** 代碼 */
    @Column(nullable = false)
    @NotNull
    private String id;

    /** 名稱 */
    @Column
    private String name;

    /** Http Method */
    @Column
    private String method;

    /** Request Matcher */
    @Column
    private String matcher;

    @ManyToMany(mappedBy = "authorities")
    private Set<Privilege> privileges;

}
