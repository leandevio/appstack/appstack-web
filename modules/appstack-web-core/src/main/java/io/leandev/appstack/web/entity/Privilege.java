package io.leandev.appstack.web.entity;

import io.leandev.appstack.web.service.PrivilegeListener;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EntityListeners(PrivilegeListener.class)
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"id"}))
/**
 * A privilege is a property of an agent, such as a user. It lets the agent do things that are not ordinarily allowed
 */
public class Privilege {
    /** 識別 */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length=36)
    @Size(max = 36)
    private String uuid;

    /** 版本 */
    @Version
    private Integer version;

    /** 代碼 */
    @Column(nullable = false)
    @NotNull
    private String id;

    /** 名稱 */
    @Column
    private String name;

    /** 授權 */
    @ManyToMany
    private Set<Permission> authorities = new HashSet<>();
}
