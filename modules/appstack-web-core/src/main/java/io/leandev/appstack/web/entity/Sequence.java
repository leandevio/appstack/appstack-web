package io.leandev.appstack.web.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="APS_SEQUENCE")
public class Sequence {
    /** 版本 */
    private Integer version;
    /** 識別 */
    private String code;
    /** 種子 */
    private long seed = 0;

    /**
     * Getter: {@link #version}.
     * @return {@link #version}.
     */
    @Version
    public Integer getVersion() {
        return version;
    }

    /**
     * Setter: {@link #version}.
     * @param version {@link #version}.
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * Getter: {@link #code}.
     * @return {@link #code}.
     */
    @Id
    @Column(length=100)
    @Size(max = 100)
    public String getCode() {
        return code;
    }

    /**
     * Setter: {@link #code}.
     * @param code {@link #code}.
     */
    public void setCode(String code) {
        this.code = code;
    }


    /**
     * Getter: {@link #seed}.
     * @return {@link #seed}.
     */
    @Column(nullable = false)
    @NotNull
    public long getSeed() {
        return seed;
    }

    public void setSeed(long seed) {
        this.seed = seed;
    }

}
