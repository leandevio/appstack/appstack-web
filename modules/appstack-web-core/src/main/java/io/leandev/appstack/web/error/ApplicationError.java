package io.leandev.appstack.web.error;

import io.leandev.appstack.exception.ApplicationException;
import org.springframework.http.HttpStatus;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ApplicationError {
    private static final String MESSAGE = "Ooops, something went terribly wrong!";
    protected String message;
    protected Object[] params;
    private String[] causes;
    private String stackTrace;

    public ApplicationError() {
        this(MESSAGE);
    }

    public ApplicationError(ApplicationException ex) {
        this.message = (ex.getMessage().equals(ex.defaultMessage())) ? defaultMessage() : ex.getMessage();
        this.params = ex.getParams();
        List<String> causes = new ArrayList<>();
        for(Throwable throwable = ex; throwable!=null; throwable = throwable.getCause()) {
            String name = throwable.getClass().getCanonicalName();
            causes.add(name + ": " + throwable.getMessage());
        }
        this.causes = causes.toArray(new String[causes.size()]);
        this.stackTrace = printStackTrace(ex);
    }

    public ApplicationError(String message, Object... params) {
        this.message = message;
        this.params = params;
    }

    public int getStatus() {
        return HttpStatus.INTERNAL_SERVER_ERROR.value();
    }

    public String getError() {
        return "Application Error";
    }

    public String getMessage() {
        return message;
    }

    public Object[] getParams() {
        return params;
    }

    public String getStackTrace() {
        return this.stackTrace;
    }

    public String defaultMessage() {
        return MESSAGE;
    }

    public String[] getCauses() {
        return this.causes;
    }

    protected String printStackTrace(ApplicationException ex) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        PrintStream writer = new PrintStream(output);
        ex.printStackTrace(writer);
        String stackTrace = new String(output.toByteArray(), StandardCharsets.UTF_8);
        writer.close();
        return stackTrace;
    }
    public String getName() {
        return this.getClass().getSimpleName();
    }
}
