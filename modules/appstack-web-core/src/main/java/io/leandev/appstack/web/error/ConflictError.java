package io.leandev.appstack.web.error;

import io.leandev.appstack.exception.ConflictException;
import org.springframework.http.HttpStatus;

public class ConflictError extends ApplicationError {
    private static final String MESSAGE = "There was a conflict detected when trying to complete your request.";

    public ConflictError() {
        this(MESSAGE);
    }
    public ConflictError(String message, Object[]... params) {
        super(message, params);
    }
    public ConflictError(ConflictException ex) {
        super(ex);
    }

    @Override
    public int getStatus() {
        return HttpStatus.CONFLICT.value();
    }

    @Override
    public String getError() {
        return "Conflict Detected";
    }


    @Override
    public String defaultMessage() {
        return MESSAGE;
    }

}
