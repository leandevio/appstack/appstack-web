package io.leandev.appstack.web.error;

import io.leandev.appstack.exception.ConstraintException;
import io.leandev.appstack.exception.Violation;
import org.springframework.http.HttpStatus;

import java.util.HashSet;
import java.util.Set;

public class ConstraintError extends ApplicationError {
    private static final String MESSAGE = "Invalid input. Please check your input and try again.";
    private Set<Violation> violations = new HashSet<>();

    public ConstraintError() {
        this(MESSAGE);
    }

    public ConstraintError(String message, Object... params) {
        super(message, params);
    }
    public ConstraintError(ConstraintException ex) {
        super(ex);
        this.violations.addAll(ex.getViolations());
    }
    @Override
    public int getStatus() {
        return HttpStatus.BAD_REQUEST.value();
    }

    @Override
    public String getError() {
        return "Validation Error";
    }

    public Set<Violation> getViolations() {
        return this.violations;
    }

    @Override
    public String defaultMessage() {
        return MESSAGE;
    }
}
