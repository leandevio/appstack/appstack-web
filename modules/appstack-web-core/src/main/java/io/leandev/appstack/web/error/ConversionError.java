package io.leandev.appstack.web.error;

import io.leandev.appstack.exception.ConversionException;
import org.springframework.http.HttpStatus;

public class ConversionError extends ApplicationError {
    private static final String MESSAGE = "There was a conversion error when trying to complete your request.";

    public ConversionError() {
        this(MESSAGE);
    }

    public ConversionError(String message, Object[]... params) {
        super(message, params);
    }
    public ConversionError(ConversionException ex) {
        super(ex);
    }

    @Override
    public int getStatus() {
        return HttpStatus.BAD_REQUEST.value();
    }

    @Override
    public String getError() {
        return "Conversion Failed";
    }

    @Override
    public String defaultMessage() {
        return MESSAGE;
    }
}
