package io.leandev.appstack.web.error;

import io.leandev.appstack.exception.DuplicateException;
import org.springframework.http.HttpStatus;

public class DuplicateError extends ApplicationError {
    private static final String MESSAGE = "Duplicate values are not allowed.";
    public DuplicateError() {
        this(MESSAGE);
    }
    public DuplicateError(String message, Object[]... params) {
        super(message, params);
    }
    public DuplicateError(DuplicateException ex) {
        super(ex);
    }

    @Override
    public int getStatus() {
        return HttpStatus.CONFLICT.value();
    }

    @Override
    public String getError() {
        return "Duplicate Value";
    }

    @Override
    public String defaultMessage() {
        return MESSAGE;
    }
}
