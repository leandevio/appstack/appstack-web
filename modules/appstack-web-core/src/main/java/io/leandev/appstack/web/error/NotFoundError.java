package io.leandev.appstack.web.error;

import io.leandev.appstack.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundError extends ApplicationError {
    private static final String MESSAGE = "The requested resource or data does not exist.";
    public NotFoundError() {
        this(MESSAGE);
    }
    public NotFoundError(String message, Object[]... params) {
        super(message, params);
    }

    public NotFoundError(NotFoundException ex) {
        super(ex);
    }

    @Override
    public int getStatus() {
        return HttpStatus.NOT_FOUND.value();
    }

    @Override
    public String getError() {
        return "Not Found";
    }

    @Override
    public String defaultMessage() {
        return MESSAGE;
    }
}
