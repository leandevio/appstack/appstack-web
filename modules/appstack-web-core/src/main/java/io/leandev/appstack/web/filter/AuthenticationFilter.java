package io.leandev.appstack.web.filter;

import io.leandev.appstack.bean.Model;
import io.leandev.appstack.env.Environ;
import io.leandev.appstack.json.JsonParser;
import io.leandev.appstack.web.constant.WebSecurityPropsKey;
import io.leandev.appstack.web.entity.Pass;
import io.leandev.appstack.web.security.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * validate the access token to verify the identity.
 */
public class AuthenticationFilter extends GenericFilterBean {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);
    private static final Environ environ = Environ.getInstance();
    private static final Boolean DISABLED = !environ.propertyAsBoolean(WebSecurityPropsKey.SECURITYENABLED);
    private static final String ANONYMOUS_PRINCIPAL = environ.property(WebSecurityPropsKey.SECURITYANONYMOUSPRINCIPAL);
    private static final String ERRORPATH = "/error";
    private static final RequestMatcher ERRORMATCHER = new AntPathRequestMatcher(ERRORPATH);

    AuthenticationService authenticationService = AuthenticationService.getDefaultInstance();

    private JsonParser jsonParser = new JsonParser();
    private Map<Class, String> ERROR_DICTIONARY = new HashMap<>();

    public AuthenticationFilter() {
        ERROR_DICTIONARY.put(AccountExpiredException.class, "ExpiredError");
        ERROR_DICTIONARY.put(DisabledException.class, "DisabledError");
        ERROR_DICTIONARY.put(LockedException.class, "LockedError");
        ERROR_DICTIONARY.put(BadCredentialsException.class, "BadCredentialsError");
        ERROR_DICTIONARY.put(InsufficientAuthenticationException.class, "InsufficientAuthenticationError");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        this.doFilter((HttpServletRequest)request, (HttpServletResponse)response, chain);
    }

    private void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        if(ERRORMATCHER.matches(request)) {
            chain.doFilter(request, response);
            return;
        }

        try {
            Authentication authenticationResult = this.attemptAuthentication(request, response);
            if(authenticationResult!=null) {
                this.successfulAuthentication(request, response, chain, authenticationResult);
            }
        } catch (InternalAuthenticationServiceException var5) {
            this.logger.error("An internal error occurred while trying to authenticate the user.", var5);
            this.unsuccessfulAuthentication(request, response, var5);
        } catch (AuthenticationException var6) {
            // default unsuccessfulAuthentication implementation will call response.sendError()
            this.unsuccessfulAuthentication(request, response, var6);
        } catch (Exception e) {
            this.logger.error("An internal error occurred while trying to authenticate the user.", e);
            this.unsuccessfulAuthentication(request, response, new InternalAuthenticationServiceException(e.getMessage(), e));
        }

        if(DISABLED && SecurityContextHolder.getContext().getAuthentication()==null) {
            // Use UsernamePasswordAuthenticationToken instead of AnonymousAuthenticationToken
            // because spring security won't pass the AnonymousAuthenticationToken to controller.
            Authentication anonymousAuthenticationToken = createAnonymousAuthenticationToken();
            SecurityContextHolder.getContext().setAuthentication(anonymousAuthenticationToken);
        }
        // let the spring security handle the rest.
        chain.doFilter(request, response);
        /** ----------------------------- --*/
    }

    private UsernamePasswordAuthenticationToken createAnonymousAuthenticationToken() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        User user = new User(ANONYMOUS_PRINCIPAL, ANONYMOUS_PRINCIPAL,
                true, true, true,
                true, authorities);
        UsernamePasswordAuthenticationToken anonymousAuthenticationToken = new UsernamePasswordAuthenticationToken(user, null, authorities);
        return anonymousAuthenticationToken;
    }

    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        Authentication authentication;
        try {
            Pass pass = this.authenticationService.authenticate(request);
            if(pass != null) {
                if(pass.getCredentials() == null) {
                    authentication = toRememberMeAuthenticationToken(pass);
                } else {
                    authentication = toUsernamePasswordAuthenticationToken(pass);
                }
            } else {
                authentication = null;
            }
        } catch (AuthenticationException e) {
            logger.info(e.getMessage(), e);
            throw e;
        }

        return authentication;
    }

    private Authentication toUsernamePasswordAuthenticationToken(Pass pass) {
        Collection<? extends GrantedAuthority> authorities = pass.getAuthorities().stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
        UserDetails userDetails = new User(pass.getPrincipal(), pass.getCredentials(),
                pass.getEnabled(), pass.getAccountNonExpired(), pass.getCredentialsNonExpired(),
                pass.getAccountNonLocked(), authorities);
        // an UsernamePasswordAuthenticationToken with userDetails is considered authenticated.
        // i.e. authentication.isAuthenticated() = true and can not be authenticated.
        // an UsernamePasswordAuthenticationToken without authentication will reveal the hashed password to the controller.
        return  new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
    }

    private Authentication toRememberMeAuthenticationToken(Pass pass) {
        Collection<? extends GrantedAuthority> authorities = pass.getAuthorities().stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
        return new RememberMeAuthenticationToken(environ.appName(), pass.getPrincipal(), authorities);
    }

    private Authentication toAnonymousAuthenticationToken(String username) {
        Collection<? extends GrantedAuthority> authorities = new HashSet<>();

        return new AnonymousAuthenticationToken(environ.appName(), username, authorities);
    }

    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication) {
        if(logger.isDebugEnabled()) {
            logger.debug(String.format("Authentication success. Updating SecurityContextHolder to contain: %s", authentication));
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    /**
     * do not send response in this method or all the custom exception handlers (configure in the SecurityContext) will have no effect.
     *
     * @param request
     * @param response
     * @param failed
     * @throws IOException
     * @throws ServletException
     */
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        SecurityContextHolder.clearContext();
        int status = HttpServletResponse.SC_UNAUTHORIZED;;
        Model<?> data = new Model<>();
        data.put("timestamp", new Date());
        data.put("status", status);
        data.put("message", failed.getMessage());
        data.put("path", request.getRequestURI());

        String error = ERROR_DICTIONARY.get(failed.getClass());
        if(error == null) {
            error = failed.getClass().getSimpleName();
        }
        data.put("error", error);
        response.setStatus(status);
        response.setContentType(MediaType.APPLICATION_JSON);
        response.setHeader("Error", error);
        jsonParser.writeValue(response.getOutputStream(), data);
    }
}