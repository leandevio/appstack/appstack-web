package io.leandev.appstack.web.filter;

import io.leandev.appstack.env.Environ;
import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;
import io.leandev.appstack.security.domain.AnonymousSecurityToken;
import io.leandev.appstack.security.domain.RememberMeSecurityToken;
import io.leandev.appstack.security.domain.SecurityToken;
import io.leandev.appstack.security.domain.UsernamePasswordSecurityToken;
import io.leandev.appstack.security.exception.UserNotFoundException;
import io.leandev.appstack.web.entity.Pass;
import io.leandev.appstack.web.security.AuthenticationService;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 *  validate the principal & credentials to verify the identity and generate the access token.
 */
public class LoginFilter extends AbstractAuthenticationProcessingFilter {
    private static final Logger LOGGER = LogManager.getLogger(LoginFilter.class);
    Environ environ = Environ.getInstance();
    private AuthenticationService authenticationService = AuthenticationService.getDefaultInstance();

    public LoginFilter(String url, AuthenticationManager authManager) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        Authentication authentication;
        try {
            Pass pass = this.authenticationService.login(request);
            if(pass.getCredentials() == null) {
                authentication = toRememberMeAuthenticationToken(pass);
            } else {
                authentication = toUsernamePasswordAuthenticationToken(pass);
            }
        } catch (AuthenticationException e) {
            logger.info(e.getMessage(), e);
            throw e;
        }

        return authentication;
    }
    private Authentication toUsernamePasswordAuthenticationToken(Pass pass) {
        Collection<? extends GrantedAuthority> authorities = pass.getAuthorities().stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
        UserDetails userDetails = new org.springframework.security.core.userdetails.User(pass.getPrincipal(), pass.getCredentials(),
                pass.getEnabled(), pass.getAccountNonExpired(), pass.getCredentialsNonExpired(),
                pass.getAccountNonLocked(), authorities);
        // an UsernamePasswordAuthenticationToken with userDetails is considered authenticated.
        // i.e. authentication.isAuthenticated() = true and can not be authenticated.
        // an UsernamePasswordAuthenticationToken without authentication will reveal the hashed password to the controller.
        return new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
    }

    private Authentication toRememberMeAuthenticationToken(Pass pass) {
        Collection<? extends GrantedAuthority> authorities = pass.getAuthorities().stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
        return new RememberMeAuthenticationToken(environ.appName(), pass.getPrincipal(), authorities);
    }


    @Override
    protected void successfulAuthentication(
            HttpServletRequest request, HttpServletResponse response,
            FilterChain chain, Authentication authentication) {
        authenticationService.successfulLogin(request, response, authentication);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException {
        SecurityContextHolder.clearContext();
        authenticationService.unsuccessfulLogin(request, response, failed);
    }

    private io.leandev.appstack.security.exception.AuthenticationException toAuthenticationException(AuthenticationException springException) {
        io.leandev.appstack.security.exception.AuthenticationException exception;
        if(springException instanceof UsernameNotFoundException) {
            exception = new UserNotFoundException(springException.getMessage(), springException);
        } else if(springException instanceof BadCredentialsException) {
            exception = new io.leandev.appstack.security.exception.BadCredentialsException(springException.getMessage(), springException);
        } else if(springException instanceof DisabledException) {
            exception = new io.leandev.appstack.security.exception.AuthenticationException(springException.getMessage(), springException);
        } else if(springException instanceof AccountExpiredException) {
            exception = new io.leandev.appstack.security.exception.AuthenticationException(springException.getMessage(), springException);
        } else if(springException instanceof LockedException) {
            exception = new io.leandev.appstack.security.exception.AuthenticationException(springException.getMessage(), springException);
        } else {
            exception = new io.leandev.appstack.security.exception.AuthenticationException(springException.getMessage(), springException);
        }

        return exception;
    }


    private Authentication toAuthentication(SecurityToken token) {
        Authentication authentication = null;
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ALL"));
        if(token instanceof AnonymousSecurityToken) {
            authentication = new AnonymousAuthenticationToken(
                    "Anonymous",
                    token.getPrincipal(),
                    authorities);
        } else if(token instanceof UsernamePasswordSecurityToken) {
            authentication = new UsernamePasswordAuthenticationToken(
                    token.principal(),
                    token.credentials(),
                    authorities);
        } else if(token instanceof RememberMeSecurityToken) {
            authentication = new AnonymousAuthenticationToken(
                    "RememberMe",
                    token.principal(),
                    authorities);
        }
        return authentication;
    }

    private SecurityToken toSecurityToken(Authentication authentication) {
        SecurityToken token = null;
        if(authentication instanceof AnonymousAuthenticationToken) {
            token = new AnonymousSecurityToken(authentication.getPrincipal());
        } else if(authentication instanceof UsernamePasswordAuthenticationToken) {
            token = new UsernamePasswordSecurityToken(authentication.getPrincipal(), authentication.getCredentials());
        }
        return token;
    }
}

