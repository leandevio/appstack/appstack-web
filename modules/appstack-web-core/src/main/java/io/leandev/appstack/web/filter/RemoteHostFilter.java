package io.leandev.appstack.web.filter;

import io.leandev.appstack.bean.Model;
import io.leandev.appstack.json.JsonParser;
import io.leandev.appstack.web.security.IpFilter;
import io.leandev.appstack.web.servlet.RequestReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Date;
import java.util.List;

@Slf4j
public class RemoteHostFilter implements Filter {
    private final IpFilter ipFilter;
    private JsonParser jsonParser = new JsonParser();
    private final RequestReader requestReader = new RequestReader();


    public RemoteHostFilter(IpFilter ipFilter) {
        this.ipFilter = ipFilter;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        this.doFilter((HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse, filterChain);
    }

    private void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        List<InetAddress> allClientAddr = requestReader.resolveAllClientAddr(request);
        for(InetAddress clientAddr : allClientAddr) {
            if(ipFilter.isBlocked(clientAddr)) {
                String reason = (clientAddr == null) ? "Could not resolve the remote host" : clientAddr.getHostAddress();
                this.unsuccessfulAuthentication(request, response, new InsufficientAuthenticationException(String.format("Remote host is blocked: %s.", reason)));
                return;
            }
        }
        chain.doFilter(request, response);
    }


    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, Exception failed) throws IOException, ServletException {
        SecurityContextHolder.clearContext();
        int status = HttpServletResponse.SC_UNAUTHORIZED;;
        Model<?> data = new Model<>();
        data.put("timestamp", new Date());
        data.put("status", status);
        data.put("message", failed.getMessage());
        data.put("path", request.getRequestURI());

        response.setStatus(status);
        response.setContentType(MediaType.APPLICATION_JSON);
        jsonParser.writeValue(response.getOutputStream(), data);
    }
}
