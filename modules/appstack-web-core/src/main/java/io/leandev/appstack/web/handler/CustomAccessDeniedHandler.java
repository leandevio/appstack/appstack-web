package io.leandev.appstack.web.handler;

import io.leandev.appstack.exception.ApplicationException;
import io.leandev.appstack.json.JsonParser;
import io.leandev.appstack.web.error.ApplicationError;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomAccessDeniedHandler implements AccessDeniedHandler {
    private JsonParser jsonParser = new JsonParser();
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)
            throws IOException, ServletException {
        ApplicationException ex = new ApplicationException(accessDeniedException.getMessage(), accessDeniedException);
        ApplicationError error = new ApplicationError(ex);

        response.setStatus(HttpStatus.FORBIDDEN.value());
        response.addHeader("Content-Type", "application/json; charset=utf-8");
        try {
            response.getWriter().write(jsonParser.writeValue(error));
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

}