package io.leandev.appstack.web.hateoas;

import org.springframework.data.domain.Page;

public class PagedResources<T> extends org.springframework.hateoas.PagedModel<T> {
    public PagedResources(Page page) {
        super(page.getContent(), new org.springframework.hateoas.PagedModel.PageMetadata(page.getSize(), page.getNumber(), page.getTotalElements()));
    }
}
