package io.leandev.appstack.web.helper;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import io.leandev.appstack.exception.*;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import sun.misc.Regexp;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.metadata.ConstraintDescriptor;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataExceptionHelper {
    private Pattern pattern = Pattern.compile("\\{([a-zA-Z_\\-]+)}");
    public ApplicationException toApplicationException(ConstraintViolationException ex) {
        ApplicationException exception;
        Set<Violation> violations = new HashSet<>();
        for(ConstraintViolation constraintViolation : ex.getConstraintViolations()) {
            String property = constraintViolation.getPropertyPath().toString();
            Map<String, Object> params = new HashMap<>();
            ResourceBundle resourceBundle = ResourceBundle.getBundle("org/hibernate/validator/ValidationMessages", Locale.ENGLISH);
            ConstraintDescriptor constraintDescriptor = constraintViolation.getConstraintDescriptor();
            String messageTemplateEL = constraintDescriptor.getMessageTemplate();
            String messageTemplate = messageTemplateEL.substring(1, messageTemplateEL.length()-1);
            String message = resourceBundle.getString(messageTemplate);
            Matcher matcher = pattern.matcher(message);
            while(matcher.find()) {
                String key = matcher.group(1);
                Object value = constraintDescriptor.getAttributes().get(key);
                params.put(key, value);
            }
            message = matcher.replaceAll("\\${$1}");
            Violation violation = new Violation(property, message, params);
            violations.add(violation);
        }
        exception = new ConstraintException(violations, ex);

        return exception;
    }


    public ApplicationException toApplicationException(DataIntegrityViolationException ex) {
        ApplicationException exception;
        Throwable cause = ex.getRootCause();
        String message = cause.getMessage();
        if(message.contains("Unique index or primary key violation")) {
            exception = new DuplicateException(ex);
        } else if(message.contains("NULL not allowed")) {
            Violation violation = new Violation("name", "NULL not allowed");
            exception = new ConstraintException(violation, ex);
        } else {
            exception = new ApplicationException(ex);
        }

        return exception;
    }

    public ApplicationException toApplicationException(ResourceNotFoundException ex) {
        return new NotFoundException(ex);
    }

    public ApplicationException toApplicationException(InvalidFormatException ex) {
        return new ConversionException(ex);
    }
}
