package io.leandev.appstack.web.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import io.leandev.appstack.web.entity.Document;
import io.leandev.appstack.web.converter.DocumentConverter;

import java.io.IOException;
import java.util.Map;

public class DocumentDeserializer extends StdDeserializer<Document> {
        DocumentConverter converter = new DocumentConverter();
        ObjectMapper mapper = new ObjectMapper();

    public DocumentDeserializer() {
            this(null);
    }

    public DocumentDeserializer(Class<?> valueType) {
            super(valueType);
            }

    @Override
    public Document deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            Map map = jsonParser.getCodec().readValue(jsonParser, Map.class);
            return converter.convert(map);
    }
}