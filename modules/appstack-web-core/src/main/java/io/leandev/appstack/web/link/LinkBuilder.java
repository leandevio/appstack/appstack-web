package io.leandev.appstack.web.link;

import org.apache.hc.core5.net.URIBuilder;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.core.AnnotationMappingDiscoverer;
import org.springframework.hateoas.server.core.CachingMappingDiscoverer;
import org.springframework.hateoas.server.core.MappingDiscoverer;
import org.springframework.hateoas.server.core.SpringAffordanceBuilder;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URI;
import java.net.URISyntaxException;

public class LinkBuilder {
    private static final MappingDiscoverer DISCOVERER = CachingMappingDiscoverer.of(new AnnotationMappingDiscoverer(RequestMapping.class));
    URIBuilder uriBuilder;

    public static LinkBuilder linkTo(Class<?> controller) {
        return new LinkBuilder(controller);
    }

    public LinkBuilder(Class<?> controller){
        String mapping = DISCOVERER.getMapping(controller);
        try {
            uriBuilder = new URIBuilder(mapping == null ? "/" : mapping);
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public LinkBuilder(URIBuilder uriBuilder){
        this.uriBuilder = uriBuilder;
    }

    public LinkBuilder slash(Object path) {
        URIBuilder uriBuilder;
        try {
            // URIBuilder.appendPath() always append a slash.
            if(this.uriBuilder.getPath().endsWith("/")) {
                // avoid double slash when URIBuild.getPath() ends with slash.
                uriBuilder = new URIBuilder(this.uriBuilder.build().toString() + path.toString());
            } else {
                uriBuilder = new URIBuilder(this.uriBuilder.build());
                uriBuilder.appendPath(path.toString());
            }
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
        return new LinkBuilder(uriBuilder);
    }

    public URI toUri() {
        return URI.create(toHref());
    }

    public String toHref() {
        String href = uriBuilder.toString();
        //@todo prefix with servletContext
        if(href.startsWith("/")) {
            href = href.substring(1);
        }
        return href;
    }

    public Link withRel(String rel) {
        return new Link(toHref(), rel);
    }

    public Link withSelfRel() {
        return new Link(toHref(), "self");
    }
}
