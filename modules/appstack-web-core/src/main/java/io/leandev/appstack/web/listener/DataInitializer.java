package io.leandev.appstack.web.listener;

import io.leandev.appstack.converter.Converters;
import io.leandev.appstack.web.entity.Code;
import io.leandev.appstack.web.entity.Document;
import io.leandev.appstack.web.service.DataInstaller;
import io.leandev.appstack.web.converter.DocumentConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer {
    @Autowired
    DataInstaller dataInstaller;

    @EventListener
    public void onApplicationReady(ApplicationReadyEvent event) {
        Converters.getDefaultInstance().register(new DocumentConverter(), Document.class);
        dataInstaller.install(Code.class);
    }
}
