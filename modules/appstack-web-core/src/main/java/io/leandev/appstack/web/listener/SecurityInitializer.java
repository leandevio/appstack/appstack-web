package io.leandev.appstack.web.listener;

import io.leandev.appstack.web.service.SecurityInstaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class SecurityInitializer {

    @Autowired
    SecurityInstaller securityInstaller;

    @EventListener
    public void onApplicationReady(ApplicationReadyEvent event) {
        securityInstaller.install();
    }
}
