package io.leandev.appstack.web.processor;

import io.leandev.appstack.processor.EnvironmentProcessor;
import io.leandev.appstack.util.Environment;

import java.util.Map;

public class WebDataEnvironmentProcessor implements EnvironmentProcessor {
    //TODO move to WebDataEnvironmentProcessor
    private static final String H2CONSOLEENABLEDKEY = "h2.console.enabled";
    private static final String H2CONSOLEPATHKEY = "h2.console.path";

    private static final String H2CONSOLEENABLED = "false";
    private static final String H2CONSOLEPATH = "/dbconsole";

    @Override
    public void postProcess(Environment environment) {
        environment.setProperty(H2CONSOLEENABLEDKEY, H2CONSOLEENABLED, false);
        environment.setProperty(H2CONSOLEPATHKEY, H2CONSOLEPATH, false);
    }

    @Override
    public void postProcess(Environment environment, Map<String, Object> props) {
        configJpa(environment, props);
        configH2(environment, props);
    }

    private void configJpa(Environment environment, Map<String, Object> props) {
        props.put("spring.jpa.open-in-view", "true");
    }

    private void configH2(Environment environment, Map<String, Object> props) {
        props.put("spring.h2.console.enabled", environment.property(H2CONSOLEENABLEDKEY));
        props.put("spring.h2.console.path", environment.property(H2CONSOLEPATHKEY));
    }

}
