package io.leandev.appstack.web.processor;

import io.leandev.appstack.processor.EnvironmentProcessor;
import io.leandev.appstack.util.Environment;
import io.leandev.appstack.web.constant.WebSecurityPropsDefault;
import io.leandev.appstack.web.constant.WebSecurityPropsKey;

import java.util.Map;

public class WebSecurityEnvironmentProcessor implements EnvironmentProcessor {

    @Override
    public void postProcess(Environment environment) {
        environment.setProperty(WebSecurityPropsKey.SECURITYLOGINPATH, WebSecurityPropsDefault.SECURITYLOGINPATH, false);
    }

    @Override
    public void postProcess(Environment environment, Map<String, Object> props) {

    }
}