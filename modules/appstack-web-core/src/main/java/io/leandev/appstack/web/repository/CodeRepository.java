package io.leandev.appstack.web.repository;

import io.leandev.appstack.web.entity.Code;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

@RestResource(exported = false)
public interface CodeRepository extends PagingAndSortingRepository<Code, String>, JpaSpecificationExecutor<Code> {
    Iterable<Code> findAllByTypeOrderByPriorityDesc(String type);
    Optional<Code> findByTypeAndCode(String type, String code);

    Optional<Code> findById(String uuid);
}
