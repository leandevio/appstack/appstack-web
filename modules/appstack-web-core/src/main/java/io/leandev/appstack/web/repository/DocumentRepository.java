package io.leandev.appstack.web.repository;

import io.leandev.appstack.web.entity.Document;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(exported = true)
public interface DocumentRepository extends PagingAndSortingRepository<Document, String> {
}
