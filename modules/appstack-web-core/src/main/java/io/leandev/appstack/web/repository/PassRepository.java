package io.leandev.appstack.web.repository;

import io.leandev.appstack.web.entity.Pass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PassRepository extends PassRepositoryCustom, JpaRepository<Pass, String>, JpaSpecificationExecutor<Pass> {
    Optional<Pass> findByPrincipal(String principle);
}
