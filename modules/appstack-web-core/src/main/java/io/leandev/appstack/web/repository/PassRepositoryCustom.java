package io.leandev.appstack.web.repository;

import io.leandev.appstack.web.entity.Pass;

public interface PassRepositoryCustom {
    Pass getByPrincipal(String principle);
}
