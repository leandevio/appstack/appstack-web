package io.leandev.appstack.web.repository;

import io.leandev.appstack.data.jpa.CriteriaQueryBuilder;
import io.leandev.appstack.data.jpa.QueryRunner;
import io.leandev.appstack.search.Predicate;
import io.leandev.appstack.web.entity.Pass;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

public class PassRepositoryImpl implements PassRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Pass getByPrincipal(String principle) {
        CriteriaQuery<Pass> criteriaQuery = CriteriaQueryBuilder.of(entityManager)
                .from(Pass.class)
                .where(Predicate.eq("principal", principle))
                .build();
        QueryRunner<Pass> queryRunner = new QueryRunner(entityManager);
        return queryRunner.findOne(criteriaQuery).orElse(null);
    }
}
