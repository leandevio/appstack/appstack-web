package io.leandev.appstack.web.repository;

import io.leandev.appstack.data.jpa.CriteriaQueryBuilder;
import io.leandev.appstack.data.jpa.QueryRunner;
import io.leandev.appstack.web.entity.Permission;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Repository
public class PermissionRepository {
    private final EntityManager entityManager;

    public Permission save(Permission permission) {
        entityManager.persist(permission);
        return permission;
    }

    public List<Permission> findAll(io.leandev.appstack.search.Predicate predicate) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Permission> criteriaQuery = CriteriaQueryBuilder.of(criteriaBuilder, Permission.class)
                .from(Permission.class)
                .where(predicate)
                .build();
        QueryRunner<Permission> queryRunner = new QueryRunner<>(entityManager);
        List<Permission> permissions = queryRunner.findAll(criteriaQuery);
        return permissions;
    }

    public Optional<Permission> find(String uuid) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Permission> criteriaQuery = criteriaBuilder.createQuery(Permission.class);
        Root<Permission> root = criteriaQuery.from(Permission.class);
        Predicate predicate = criteriaBuilder.equal(root.get("uuid"), uuid);
        criteriaQuery.where(predicate);

        Permission permission = entityManager.createQuery(criteriaQuery).getSingleResult();

        return Optional.ofNullable(permission);
    }
    public Optional<Permission> findById(String id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Permission> criteriaQuery = criteriaBuilder.createQuery(Permission.class);
        Root<Permission> root = criteriaQuery.from(Permission.class);
        Predicate predicate = criteriaBuilder.equal(root.get("id"), id);
        criteriaQuery.where(predicate);

        Permission permission = entityManager.createQuery(criteriaQuery).getSingleResult();

        return Optional.ofNullable(permission);
    }
}
