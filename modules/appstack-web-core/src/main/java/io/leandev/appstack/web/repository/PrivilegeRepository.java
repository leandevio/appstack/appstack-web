package io.leandev.appstack.web.repository;

import io.leandev.appstack.data.jpa.CriteriaQueryBuilder;
import io.leandev.appstack.data.jpa.QueryRunner;
import io.leandev.appstack.web.entity.Privilege;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Repository
public class PrivilegeRepository {
    private final EntityManager entityManager;

    public Privilege save(Privilege privilege) {
        entityManager.persist(privilege);
        return privilege;
    }

    public List<Privilege> findAll(io.leandev.appstack.search.Predicate predicate) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Privilege> criteriaQuery = CriteriaQueryBuilder.of(criteriaBuilder, Privilege.class)
                .from(Privilege.class)
                .where(predicate)
                .build();
        QueryRunner<Privilege> queryRunner = new QueryRunner<>(entityManager);
        List<Privilege> privileges = queryRunner.findAll(criteriaQuery);
        return privileges;
    }

    public Optional<Privilege> find(String uuid) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Privilege> criteriaQuery = criteriaBuilder.createQuery(Privilege.class);
        Root<Privilege> root = criteriaQuery.from(Privilege.class);
        Predicate predicate = criteriaBuilder.equal(root.get("uuid"), uuid);
        criteriaQuery.where(predicate);

        Privilege privilege = entityManager.createQuery(criteriaQuery).getSingleResult();

        return Optional.ofNullable(privilege);
    }
    public Optional<Privilege> findById(String id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Privilege> criteriaQuery = criteriaBuilder.createQuery(Privilege.class);
        Root<Privilege> root = criteriaQuery.from(Privilege.class);
        Predicate predicate = criteriaBuilder.equal(root.get("id"), id);
        criteriaQuery.where(predicate);

        Privilege privilege = entityManager.createQuery(criteriaQuery).getSingleResult();

        return Optional.ofNullable(privilege);
    }
}
