package io.leandev.appstack.web.repository;

import io.leandev.appstack.web.entity.Sequence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(exported = false)
public interface SequenceRepository extends JpaRepository<Sequence, String> {
}
