package io.leandev.appstack.web.security;

import io.leandev.appstack.bean.Model;
import io.leandev.appstack.crypto.PasswordEncoder;
import io.leandev.appstack.json.JsonParser;
import io.leandev.appstack.web.constant.WebSecurityPropsDefault;
import io.leandev.appstack.web.constant.WebSecurityPropsKey;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractAuthenticationService implements AuthenticationService {
    private static final PasswordEncoder passwordEncoder = PasswordEncoder.getDefaultInstance();
    private long EXPIRATIONTIME = environ.propertyAsLong(WebSecurityPropsKey.SECURITYTOKENTTL, WebSecurityPropsDefault.SECURITYTOKENTTL);
    private AuthenticationService authenticationService = AuthenticationService.getDefaultInstance();
    private static Map<Class, String> ERROR_DICTIONARY = new HashMap<>();
    private JsonParser jsonParser = new JsonParser();

    static {
        ERROR_DICTIONARY.put(AccountExpiredException.class, "ExpiredError");
        ERROR_DICTIONARY.put(DisabledException.class, "DisabledError");
        ERROR_DICTIONARY.put(LockedException.class, "LockedError");
        ERROR_DICTIONARY.put(BadCredentialsException.class, "BadCredentialsError");
        ERROR_DICTIONARY.put(InsufficientAuthenticationException.class, "InsufficientAuthenticationError");
    }

    /**
     * create the access token and add to the response headers.
     *
     * @param request
     * @param response
     * @param authentication
     */
    @Override
    public void successfulLogin(HttpServletRequest request, HttpServletResponse response,  Authentication
            authentication) {
        String authorizationToken = createAuthorizationToken(authentication.getName(), EXPIRATIONTIME);
        response.addHeader("Authorization", authorizationToken);
        response.addHeader("Access-Control-Expose-Headers", "Authorization");
    }

    @Override
    public void unsuccessfulLogin(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException {
        int status = HttpServletResponse.SC_OK;
        Model<?> data = new Model<>();
        data.put("timestamp", new Date());
        data.put("status", status);
        data.put("message", failed.getMessage());
        data.put("path", request.getRequestURI());

        String error = ERROR_DICTIONARY.get(failed.getClass());
        if(error == null) {
            error = failed.getClass().getSimpleName();
        }
        data.put("error", error);
        response.setStatus(status);
        response.setContentType(MediaType.APPLICATION_JSON);
        response.setHeader("Error", error);
        jsonParser.writeValue(response.getOutputStream(), data);
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        boolean match = false;
        if(rawPassword==null || encodedPassword==null) {
            match = (rawPassword==null && encodedPassword==null);
        } else if(passwordEncoder.matches(rawPassword, encodedPassword)) {
            match = true;
        }
        return match;
    }
}
