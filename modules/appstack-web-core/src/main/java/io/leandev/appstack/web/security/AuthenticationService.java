package io.leandev.appstack.web.security;


import io.leandev.appstack.env.Environ;
import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;
import io.leandev.appstack.security.constant.SecurityPropsKey;
import io.leandev.appstack.security.domain.SecurityToken;
import io.leandev.appstack.security.domain.UserInfo;
import io.leandev.appstack.util.FactoryProps;
import io.leandev.appstack.web.entity.Pass;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface AuthenticationService {
    Logger LOGGER = LogManager.getLogger(AuthenticationService.class);

    String AUTHENTICATIONSERVICE = "io.leandev.appstack.web.security.JWTAuthenticationService";

    Environ environ = Environ.getInstance();
    String SIGNING_KEY = environ.property(SecurityPropsKey.SECURITYTOKEYKEY);
    long EXPIRATION_TIME = environ.propertyAsLong(SecurityPropsKey.SECURITYTOKENTTL); // in milliseconds


    Pass authenticate(HttpServletRequest request);

    Pass login(HttpServletRequest request);

    default void successfulLogin(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
    }

    default void unsuccessfulLogin(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException {
        response.sendError(HttpStatus.UNAUTHORIZED.value(), failed.getMessage());
    }

    String createAuthorizationToken(Object principal, long expiration);

    String createAccessToken(String principal, long expiration, String signingKey);
    default String createAccessToken(String principal, long expiration) {
        return createAccessToken(principal, expiration, SIGNING_KEY);
    }
    default String createAccessToken(String principal) {
        return createAccessToken(principal, EXPIRATION_TIME);
    }
    String parseAccessToken(String token, String signingKey);

    default String parseAccessToken(String token) {
        return parseAccessToken(token, SIGNING_KEY);
    }

    UserInfo getUserInfo(Object principal);

    boolean matches(CharSequence rawPassword, String encodedPassword);

    Pass authenticateSecurityToken(String token);

    Pass authenticateAccessToken(String token);
    class SingletonHolder {
        static final AuthenticationService INSTANCE = AuthenticationService.getInstance();
    }

    static AuthenticationService getDefaultInstance() {
        return SingletonHolder.INSTANCE;
    }

    static AuthenticationService getInstance() {
        AuthenticationService authenticationService;
        Class<? extends AuthenticationService> type;
        String name = FactoryProps.getDefaultInstance().property(AuthenticationService.class, AUTHENTICATIONSERVICE);
        try {
            type = (Class<? extends AuthenticationService>) Class.forName(name);
            authenticationService = type.newInstance();
        } catch (Exception e) {
            LOGGER.error(String.format("Failed to instantiate %s. Check the setting(%s) in the META-INF/appstack.factories."
                    , name, AuthenticationService.class.getCanonicalName()), e);
            throw new RuntimeException(e);
        }

        return authenticationService;
    }
}
