package io.leandev.appstack.web.security;


import io.leandev.appstack.net.InetAddressRange;
import lombok.extern.slf4j.Slf4j;

import java.net.InetAddress;
import java.util.HashSet;
import java.util.Set;

/**
 * IP Filter
 * block those ip addresses in the blacklist except those in the whitelist.
 * ip range could be in the following format:
 * - CIDR notation
 * - single ip address
 */
@Slf4j
public class IpFilter {
    private Set<InetAddressRange> blacklist = new HashSet<>();
    private Set<InetAddressRange> whitelist = new HashSet<>();

    public boolean isBlocked(InetAddress inetAddress) {
        return !blacklist.isEmpty() && this.inRange(inetAddress, this.blacklist) && !this.inRange(inetAddress, this.whitelist);
    }

    public boolean isPermitted(InetAddress inetAddress) {
        return !isBlocked(inetAddress);
    }

    private boolean inRange(InetAddress inetAddress, Set<InetAddressRange> inetAddressRanges) {
        return inetAddressRanges.stream().anyMatch(inetAddressRange -> inetAddressRange.inRange(inetAddress));
    }

    public void permitAll() {
        this.reset();
    }

    public boolean permit(String ipAddressRange) {
        whitelist.add(InetAddressRange.of(ipAddressRange));
        return true;
    }

    public void permitAll(String... ipRanges) {
        for(String ipRange : ipRanges) {
            this.permit(ipRange);
        }
    }

    public void denyAll() {
        this.reset();
        this.deny(InetAddressRange.ENTIRE_NETWORK);
    }

    public void denyAll(String... ipAddressRanges) {
        for(String ipAddressRange : ipAddressRanges) {
            this.deny(ipAddressRange);
        }
    }

    public boolean deny(String ipAddressRange) {
        blacklist.add(InetAddressRange.of(ipAddressRange));
        return true;
    }

    private void reset() {
        whitelist.clear();
        blacklist.clear();
    }

}
