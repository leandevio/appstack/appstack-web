package io.leandev.appstack.web.security;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.leandev.appstack.bean.ObjectCopier;
import io.leandev.appstack.bean.ObjectReadWriteException;
import io.leandev.appstack.env.Environ;
import io.leandev.appstack.nls.I18n;
import io.leandev.appstack.security.constant.SecurityPropsKey;
import io.leandev.appstack.web.entity.Pass;
import io.leandev.appstack.security.domain.*;
import io.leandev.appstack.security.exception.BadCredentialsException;
import io.leandev.appstack.web.config.WebConfiguration;
import io.leandev.appstack.web.constant.WebSecurityPropsDefault;
import io.leandev.appstack.web.constant.WebSecurityPropsKey;
import io.leandev.appstack.web.service.PassService;
import io.leandev.appstack.web.servlet.RequestReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Slf4j
public class JWTAuthenticationService extends AbstractAuthenticationService {
    private static final Environ environ = Environ.getInstance();
    private long EXPIRATIONTIME = environ.propertyAsLong(WebSecurityPropsKey.SECURITYTOKENTTL, WebSecurityPropsDefault.SECURITYTOKENTTL);
    private String SIGNINGKEY = environ.property(WebSecurityPropsKey.SECURITYTOKEYKEY, WebSecurityPropsDefault.SECURITYTOKENKEY);
    private static final String BEARER = "Bearer", BASIC = "Basic";
    private String SECURITYANONYMOUSPRINCIPAL = environ.property(WebSecurityPropsKey.SECURITYANONYMOUSPRINCIPAL, WebSecurityPropsDefault.SECURITYANONYMOUSPRINCIPAL);

    private static final String WILDCARD = environ.property(SecurityPropsKey.SECURITYWILDCARD);
    private static final Boolean DISABLED = !environ.propertyAsBoolean(SecurityPropsKey.SECURITYENABLED);
    private static final String[] PRINCIPALS = environ.propertyAsArray(SecurityPropsKey.SECURITYPRINCIPALS);

    private String anonymous = environ.property(WebSecurityPropsKey.SECURITYANONYMOUSPRINCIPAL, WebSecurityPropsDefault.SECURITYANONYMOUSPRINCIPAL);
    private RequestReader requestReader = new RequestReader();

    private ConcurrentMap<String, Integer> failedAttempt = new ConcurrentHashMap<>();
    private ConcurrentMap<String, LocalDateTime> lockoutList = new ConcurrentHashMap<>();
    private int LOCKOUT_THRESHOLD = 5;
    private I18n i18n = I18n.getDefaultInstance();

    public JWTAuthenticationService() {}

    /**
     * authenticate the request
     *
     * @param request
     * @return
     */
    @Override
    public Pass authenticate(HttpServletRequest request) {
        Pass pass = null;
        String authorization = request.getHeader("Authorization");
        /** -- username/password or token authentication --*/
        if(authorization != null) {
            String[] params = authorization.split(" ");
            String scheme = params[0];
            String token = params[1];

            if(scheme.equals(BASIC)) {
                pass = authenticateSecurityToken(token);
            } else if(scheme.equals(BEARER)) {
                pass = authenticateAccessToken(token);
            } else {
                throw new org.springframework.security.authentication.BadCredentialsException(String.format("Unknown authorization scheme: `%s`", authorization));
            }
        }
        /** --------------------------------*/

        /** -- IP address restriction --*/
        if(pass != null && pass.getIpRestrictions() != null) {
            String[] ipRestrictions = pass.getIpRestrictions().split(",");
            IpFilter ipFilter = new IpFilter();
            ipFilter.denyAll();
            ipFilter.permitAll(ipRestrictions);
            List<InetAddress> allClientAddr = requestReader.resolveAllClientAddr(request);
            for(InetAddress clientAddr : allClientAddr) {
                if(ipFilter.isBlocked(clientAddr)) {
                    throw new InsufficientAuthenticationException(String.format("Remote host is blocked by ip address restriction(%s): %s", pass.getPrincipal(), clientAddr));
                }
            }
        }
        /** --------------------------------*/

        /** -- IP address authentication --*/
        if(pass == null) {
            Optional<InetAddress> remoteAddr = requestReader.resolveRemoteAddr(request);
            if(remoteAddr.isPresent()) {
                pass = authenticateRemoteHost(remoteAddr.get());
            }
        }
        /** --------------------------------*/
        if(pass == null) {
//            throw new InsufficientAuthenticationException("Unable to authenticate the request");
        } else {
            if(!pass.getEnabled()) {
                throw new DisabledException("The user account has not been activated yet.");
            } else if(!pass.getAccountNonExpired()) {
                throw new AccountExpiredException("The user account has expired.");
            } else if(!pass.getAccountNonLocked()) {
                throw new LockedException("The user account has been suspended.");
            }
        }
        return pass;
    }

    @Override
    public Pass login(HttpServletRequest request) {
        return authenticate(request);
    }

    @Override
    public Pass authenticateSecurityToken(String token) {
        PassService passService = this.getPassService();
        String passport;
        try {
            passport = new String(DatatypeConverter.parseBase64Binary(token), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new org.springframework.security.authentication.BadCredentialsException(e.getMessage(), e);
        }

        String[] params = passport.split(":");
        String principal = params[0];
        String credentials = params.length > 1 ? params[1] : "";
        if(log.isDebugEnabled()) {
            log.debug(String.format("Try to authenticate by the security token: %s(%s)", principal, token));
        }
        if(principal.length() == 0 || credentials.length() == 0) {
            throw new org.springframework.security.authentication.BadCredentialsException("Username & password must not be empty.");
        }

        if(this.isLockout(principal)) {
            Duration duration = this.getLockoutDuration(principal);
            throw new SecurityException("Too many failed login attempts. The user gets locked for %d minutes.", duration.toMinutes());
        }

        // allow to use other properties(mobile, email ...) as user identity.
        Optional<Pass> pass = passService.findOne(principal, PRINCIPALS);
        pass.orElseThrow(() -> new org.springframework.security.authentication.BadCredentialsException(String.format("User not found: %s.", principal)));

        if(!(DISABLED || WILDCARD != null && WILDCARD.equals(credentials))) {
            if(!this.matches(credentials, pass.get().getCredentials())) {
                int count = this.logFailedLoginAttempt(principal);
                if(this.isLockout(principal)) {
                    throw new SecurityException("Too many failed login attempts. The user gets locked for %d minutes.", count);
                } else {
                    String message = String.format(i18n.translate("Username and password do not match."), count);
                    throw new org.springframework.security.authentication.BadCredentialsException(message);
                }
            } else {
                this.clearFailedLoginAttempt(principal);
            }
        }

        if(log.isDebugEnabled()) {
            log.debug(String.format("Authenticated by the security token: %s(%s)", principal, token));
        }

        return pass.get();
    }

    private boolean isLockout(String principal) {
        LocalDateTime lockoutTime = this.lockoutList.get(principal);
        return (lockoutTime != null && lockoutTime.isAfter(LocalDateTime.now()));
    }

    private Duration getLockoutDuration(String principal) {
        LocalDateTime lockoutTime = this.lockoutList.get(principal);
        Duration duration = Duration.between(LocalDateTime.now(), lockoutTime);
        return duration;
    }

    private int logFailedLoginAttempt(String principal) {
        Integer count = this.failedAttempt.getOrDefault(principal, 0);
        this.failedAttempt.put(principal, ++count);
        if(count >= LOCKOUT_THRESHOLD) {
            LocalDateTime lockoutTime = LocalDateTime.now().plusMinutes(count);
            this.lockoutList.put(principal, lockoutTime);
        }
        return count;
    }

    private void clearFailedLoginAttempt(String principal) {
        this.failedAttempt.remove(principal);
        this.lockoutList.remove(principal);
    }

    @Override
    public Pass authenticateAccessToken(String token) {
        PassService passService = this.getPassService();
        String principal = parseAccessToken(token);
        if(log.isDebugEnabled()) {
            log.debug(String.format("Try to authenticate by the access token: %s(%s)", principal, token));
        }
        Optional<Pass> pass = passService.findByPrincipal(principal);
        pass.orElseThrow(() -> new org.springframework.security.authentication.BadCredentialsException(String.format("User not found: %s.", principal)));
        ObjectCopier objectCopier = new ObjectCopier();
        Pass _pass = objectCopier.copyObject(pass.get());
        _pass.setCredentials(null);

        if(log.isDebugEnabled()) {
            log.debug(String.format("Authenticated by the access token: %s(%s)", principal, token));
        }

        return _pass;
    }

    public Pass authenticateRemoteHost(InetAddress remoteHost) {
        PassService passService = this.getPassService();
        String principal = remoteHost.getHostAddress();
        if(log.isDebugEnabled()) {
            log.debug(String.format("Try to authenticate by ip address(host address): %s", principal));
            log.debug(String.format("The host address of the remote host being authenticated: %s", remoteHost.getHostAddress()));
            log.debug(String.format("The address of the remote host being authenticated: %s", remoteHost.getAddress()));
            log.debug(String.format("The hostname of the remote host being authenticated: %s", remoteHost.getHostName()));
            log.debug(String.format("The canonical hostname of the remote host being authenticated: %s", remoteHost.getCanonicalHostName()));
        }
        Optional<Pass> pass = passService.findByPrincipal(principal);
        if(!pass.isPresent()) {
            return null;
        }
        ObjectCopier objectCopier = new ObjectCopier();
        Pass _pass = objectCopier.copyObject(pass.get());
        _pass.setCredentials(null);
        if(log.isDebugEnabled()) {
            log.debug(String.format("Authenticated by the ip address: %s", principal));
        }
        return _pass;
    }

    @Override
    public String createAuthorizationToken(Object principal, long expiration) {
        String subject = principal.toString();
        String jwtToken = this.createAccessToken(subject, expiration, SIGNINGKEY);
        return BEARER + " " + jwtToken;
    }

    @Override
    public UserInfo getUserInfo(Object principal) {
        PassService passService = this.getPassService();
        Optional<Pass> $pass = passService.findByPrincipal(principal.toString());
        Pass pass = $pass.orElseThrow(() -> new UsernameNotFoundException("Account not found."));

        UserInfo userInfo = new UserInfo(pass.getPrincipal(), pass.getCredentials());
        userInfo.setActivationDate(pass.getActivationDate());
        userInfo.setExpiryDate(pass.getExpiryDate());
        userInfo.setSuspensionDate(pass.getSuspensionDate());

        if(!userInfo.isActivated()) {
            throw new DisabledException("The user account has not been activated yet.");
        } else if(userInfo.isExpired()) {
            throw new AccountExpiredException("The user account has expired.");
        } else if(userInfo.isSuspended()) {
            throw new LockedException("The user account has been suspended.");
        }

        return userInfo;
    }

    private PassService getPassService() {
        return WebConfiguration.getBean(PassService.class);
    }

    @Override
    public String createAccessToken(String principal, long expiration, String signingKey) {
        String subject = principal;
        SecretKey secretKey  = Keys.hmacShaKeyFor(signingKey.getBytes(StandardCharsets.UTF_8));
        JwtBuilder jwtBuilder = Jwts.builder()
                .setIssuer(environ.appName())
                .setSubject(subject)
                .setIssuedAt(new Date())
                .signWith(secretKey, SignatureAlgorithm.HS256);
        if(expiration >= 0) {
            jwtBuilder.setExpiration(new Date(System.currentTimeMillis() + expiration));
        }
        String jwtToken = jwtBuilder.compact();
        return jwtToken;
    }

    @Override
    public String parseAccessToken(String token, String signingKey) {
        SecretKey secretKey  = Keys.hmacShaKeyFor(signingKey.getBytes(StandardCharsets.UTF_8));
        String principal = Jwts.parserBuilder()
                .setSigningKey(secretKey)
                .build()
                .parseClaimsJws(token)
                .getBody()
                .getSubject();

        return principal;
    }

    public boolean isBasicAuthScheme(HttpServletRequest request) {
        return (BASIC.equals(this.getAuthScheme(request)));
    }

    public String getAuthScheme(HttpServletRequest request) {
        String authorization = request.getHeader("Authorization");
        if(authorization == null) {
            return null;
        }
        String[] tokens = authorization.split(" ");
        return tokens[0];
    }

    private SecurityToken getBasicSchemeSecurityToken(String hash) {
        String s;
        try {
            s = new String(DatatypeConverter.parseBase64Binary(hash), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new BadCredentialsException(e);
        }
        String[] params = s.split(":");
        String principals = params[0];
        String credentials = params.length>1 ? params[1] : null;

        if(principals==null) return new AnonymousSecurityToken(SECURITYANONYMOUSPRINCIPAL);

        return new UsernamePasswordSecurityToken(principals, credentials);
    }

    private SecurityToken getBearerSchemeSecurityToken(String token) {
        String principal = this.parseAccessToken(token, SIGNINGKEY);

        return new RememberMeSecurityToken(principal);
    }
}
