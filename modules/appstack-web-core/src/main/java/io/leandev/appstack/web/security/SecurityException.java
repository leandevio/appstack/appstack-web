package io.leandev.appstack.web.security;

import lombok.Getter;
import org.springframework.security.core.AuthenticationException;

@Getter
public class SecurityException extends AuthenticationException {
    private String format;
    private Object[] params;

    public SecurityException(String message, Object... params) {
        super(String.format(message, params));
        this.format = message;
        this.params = params;
    }

    public SecurityException(String message, Throwable cause, Object... params) {
        super(String.format(message, params), cause);
        this.format = message;
        this.params = params;
    }
}
