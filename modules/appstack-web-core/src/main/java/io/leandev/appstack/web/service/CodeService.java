package io.leandev.appstack.web.service;

import io.leandev.appstack.cache.CacheBuilder;
import io.leandev.appstack.data.domain.SpecificationBuilder;
import io.leandev.appstack.data.jpa.PageableBuilder;
import io.leandev.appstack.search.Predicate;
import io.leandev.appstack.web.entity.Code;
import io.leandev.appstack.web.repository.CodeRepository;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Service
@Transactional
public class CodeService {

    private CodeRepository repository;
    private Cache<String, Object> cache;

    @Autowired
    public CodeService(CodeRepository repository, Optional<CacheManager> cacheManager) {
        this.repository = repository;
        if(cacheManager.isPresent()) {
            String name = this.getClass().getCanonicalName();
            cache = CacheBuilder.newCache(cacheManager.get(), name, String.class, Object.class)
                    .heap(50)
                    .offheap(50)
                    .build();
        }
    }

    public Page<Code> findAll(Specification<Code> spec, Pageable pageable) {
        return repository.findAll(spec, pageable);
    }

    public Page<Code> findAll(Predicate predicate, Pageable pageable) {
        pageable = PageableBuilder.of(pageable)
                .withDefault(Sort.by(Sort.Order.desc("priority")))
                .build();
        Specification<Code> spec = SpecificationBuilder.newInstance()
                .from(Code.class)
                .where(predicate)
                .build();
        return repository.findAll(spec, pageable);
    }

    public Iterable<Code> findAllByType(String type) {
        Iterable<Code> codes;
        if(cache != null && cache.containsKey(type)) {
            codes = (Iterable<Code>) cache.get(type);
        } else {
            codes = repository.findAllByTypeOrderByPriorityDesc(type);
            // cache the codes only if there are data.
            if(cache != null && codes.iterator().hasNext()) {
                cache.put(type, codes);
            }
        }
        return codes;
    }

    public Optional<Code> findByTypeAndCode(String type, String code) {
        return repository.findByTypeAndCode(type, code);
    }

    public Code save(Code code) {
        if(cache.containsKey(code.getType())) {
            cache.remove(code.getType());
        }
        return repository.save(code);
    }

    public void delete(String type, String code) {
        if(cache.containsKey(type)) {
            cache.remove(type);
        }
        Optional<Code> bean = repository.findByTypeAndCode(type, code);
        if(bean.isPresent()) repository.delete(bean.get());
    }

    public Optional<Code> findByTypeAndValue(String type, String value) {
        Optional<Code> code;
        if(cache != null) {
            code = StreamSupport.stream(findAllByType(type).spliterator(), false)
                    .filter($code -> $code.getValue().equals(value)).findFirst();
        } else {
            code = repository.findByTypeAndCode(type, value);
        }

        return code;
    }
}
