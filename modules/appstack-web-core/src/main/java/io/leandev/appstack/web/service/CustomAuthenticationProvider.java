package io.leandev.appstack.web.service;

import io.leandev.appstack.security.domain.UserInfo;
import io.leandev.appstack.util.Environment;
import io.leandev.appstack.web.constant.WebSecurityPropsDefault;
import io.leandev.appstack.web.constant.WebSecurityPropsKey;
import io.leandev.appstack.web.security.AuthenticationService;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
    private static final Environment environment = Environment.getDefaultInstance();
    private static final boolean SECURITYANONYMOUSENABLED = environment.propertyAsBoolean(WebSecurityPropsKey.SECURITYANONYMOUSENABLED, WebSecurityPropsDefault.SECURITYANONYMOUSENABLED);
    private static final AuthenticationService authenticationService = AuthenticationService.getDefaultInstance();
    /**
     * Performs authentication with the same contract as AuthenticationManager.authenticate(Authentication) .
     * @param authentication
     * @return
     * @throws AuthenticationException
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if(authentication instanceof AnonymousAuthenticationToken) {
            return validate((AnonymousAuthenticationToken) authentication);
        } else if(authentication instanceof UsernamePasswordAuthenticationToken) {
            return validate((UsernamePasswordAuthenticationToken) authentication);
        } else if(authentication instanceof RememberMeAuthenticationToken) {
            return validate((RememberMeAuthenticationToken) authentication);
        }
        throw new AuthenticationServiceException(String.format("Authentication token(%s) not support.", authentication.getClass().getName()));
    }

    private Authentication validate(AnonymousAuthenticationToken authentication) {
        if("RememberMe".hashCode()==authentication.getKeyHash()) {
            String username = authentication.getPrincipal().toString();
            authenticationService.getUserInfo(username);
            return authentication;
        } else if("Anonymous".hashCode()==authentication.getKeyHash()) {
            if(SECURITYANONYMOUSENABLED) {
                return authentication;
            } else {
                throw new BadCredentialsException("The principal can not be null");
            }
        }
        throw new BadCredentialsException("Unauthorized client.");
    }

    private Authentication validate(UsernamePasswordAuthenticationToken authentication) {
        String username = authentication.getPrincipal().toString();
        String password = (authentication.getCredentials()==null) ? null : authentication.getCredentials().toString();

        UserInfo userInfo = authenticationService.getUserInfo(username);

        if(authenticationService.matches(password, userInfo.getPassword())) {
            return authentication;
        } else {
            throw new BadCredentialsException("Credentials mismatch.");
        }
    }

    private Authentication validate(RememberMeAuthenticationToken authentication) {
        return authentication;
    }

    /**
     * Returns true if this AuthenticationProvider supports the indicated Authentication object.
     * @param authentication
     * @return
     */
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class)||
                authentication.equals(AnonymousAuthenticationToken.class);

    }
}