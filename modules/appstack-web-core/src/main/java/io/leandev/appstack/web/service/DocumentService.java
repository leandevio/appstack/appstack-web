package io.leandev.appstack.web.service;


import io.leandev.appstack.web.entity.Document;
import io.leandev.appstack.web.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class DocumentService {
    private final DocumentRepository repository;

    @Autowired
    public DocumentService(DocumentRepository repository) {
        this.repository = repository;
    }

    public Page<Document> findAll(Pageable pageable) {
        return  repository.findAll(pageable);
    }

    public Iterable<Document> findAll() {
        return repository.findAll();
    }

    public Optional<Document> findById(String uuid) {
        return repository.findById(uuid);
    }
}
