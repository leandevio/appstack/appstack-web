package io.leandev.appstack.web.service;

import io.leandev.appstack.web.config.WebSecurityConfiguration;
import io.leandev.appstack.web.entity.Pass;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import java.util.Optional;

@Slf4j
public class PassListener {
    @PreUpdate
    @PreRemove
    private void beforeAnyUpdate(Pass pass) {
        Optional<PassService> passService = WebSecurityConfiguration.getBean(PassService.class);
        if(passService.isPresent()) {
            passService.get().handleBeforeAnyUpdate(pass);
        }
    }
}
