package io.leandev.appstack.web.service;

import io.leandev.appstack.bean.Model;
import io.leandev.appstack.cache.CacheBuilder;
import io.leandev.appstack.data.domain.SpecificationBuilder;
import io.leandev.appstack.exception.NotFoundException;
import io.leandev.appstack.search.Predicate;
import io.leandev.appstack.web.entity.Pass;
import io.leandev.appstack.web.repository.PassRepository;
import lombok.extern.slf4j.Slf4j;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * If more than one spring service implement the UserDetailService, there must be one and only one to register with the name - userDetailService.
 * If not, the authenticationManager bean will be null.
 */
@Slf4j
@Service
public class PassService implements UserDetailsService {

    private final PassRepository passRepository;
    private final PermissionService permissionService;
    // key: principal
    private Cache<String, Pass> cache = null;

    public PassService(PassRepository passRepository, PermissionService permissionService, Optional<CacheManager> cacheManager) {
        this.passRepository = passRepository;
        this.permissionService = permissionService;
        if(cacheManager.isPresent()) {
            String name = this.getClass().getCanonicalName();
            // @todo 自動調整快取大小
            cache = CacheBuilder.newCache(cacheManager.get(), name, String.class, Pass.class)
                    .heap(1000)
                    .offheap(100000)
                    .build();
        }
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.debug("Getting user info via PassService");
        Pass pass = findByPrincipal(username).orElseThrow(() -> new UsernameNotFoundException(String.format("User name %s not found", username)));
        return new org.springframework.security.core.userdetails.User(pass.getPrincipal(), pass.getCredentials(),
                pass.getEnabled(), pass.getAccountNonExpired(), pass.getCredentialsNonExpired(),
                pass.getAccountNonLocked(), convertToSpringAuthorities(pass.getAuthorities()));
    }

    private Collection<? extends GrantedAuthority> convertToSpringAuthorities(Set<String> permissions) {
        return permissions.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
    }

    public Optional<Pass> findByPrincipal(String principal) {
        return findOne(principal);
    }

    public Optional<Pass> findOne(String principal, String... props) {
        Optional<Pass> pass = Optional.empty();
        if(cache != null) {
            pass = Optional.ofNullable(cache.get(principal));
            if(!pass.isPresent() && props.length > 0) {
                java.util.Iterator<Cache.Entry<String, Pass>> iterable = cache.iterator();
                while (iterable.hasNext()) {
                    Cache.Entry<String, Pass> entry = iterable.next();
                    Model<Pass> model = new Model(entry.getValue());
                    boolean found = Arrays.asList(props).stream().anyMatch(prop -> principal.equalsIgnoreCase(model.getString(prop)));
                    if (found) {
                        pass = Optional.of(model.getBean());
                        break;
                    }
                }
            }
        }

        if(!pass.isPresent()) {
            Predicate predicate = Predicate.eq("principal", principal);
            for(String id : props) {
                predicate = predicate.or(Predicate.eq(id, principal));
            }
            pass = this.findOne(predicate);
            if(cache != null && pass.isPresent()) {
                cache.put(principal, pass.get());
            }
        }

        return pass;
    }

    public Optional<Pass> findOne(Predicate predicate) {
        Specification<Pass> spec = SpecificationBuilder.newInstance()
                .from(Pass.class)
                .where(predicate)
                .build();
        return passRepository.findOne(spec);
    }

    @Transactional
    public Pass save(Pass pass) {
        return passRepository.save(pass);
    }

    @Transactional
    public void delete(Pass pass) {
        passRepository.delete(pass);
    }

    @Transactional
    public void deleteByPrincipal(String principal) {
        Optional<Pass> pass = passRepository.findByPrincipal(principal);
        if(pass.isPresent()) {
            passRepository.delete(pass.get());
        } else {
            log.warn(String.format("Pass(%s) not found.", principal));
        }
    }

    public void handleBeforeAnyUpdate(Pass pass) {
        if(cache != null) {
            cache.remove((pass.getPrincipal()));
        }
    }

    public Pass saveToCache(Pass pass) {
        this.cache.put(pass.getPrincipal(), pass);
        return pass;
    }
}
