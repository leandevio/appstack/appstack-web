package io.leandev.appstack.web.service;

import io.leandev.appstack.web.config.WebSecurityConfiguration;
import io.leandev.appstack.web.entity.Permission;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import java.util.Optional;

@Slf4j
public class PermissionListener {
    @PreUpdate
    @PreRemove
    private void beforeAnyUpdate(Permission permission) {
        Optional<PermissionService> permissionService = WebSecurityConfiguration.getBean(PermissionService.class);
        if(permissionService.isPresent()) {
            permissionService.get().handleBeforeAnyUpdate(permission);
        }
    }
}
