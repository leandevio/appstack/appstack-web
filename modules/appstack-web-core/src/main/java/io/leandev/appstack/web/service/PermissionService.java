package io.leandev.appstack.web.service;

import io.leandev.appstack.cache.CacheBuilder;
import io.leandev.appstack.env.Environ;
import io.leandev.appstack.search.Predicate;
import io.leandev.appstack.web.entity.Permission;
import io.leandev.appstack.web.repository.PermissionRepository;
import lombok.extern.slf4j.Slf4j;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class PermissionService {

    private static final Environ environ = Environ.getInstance();

    private final PermissionRepository permissionRepository;
    // key: uuid or id
    private Cache<String, Permission> cache = null;

    public PermissionService(PermissionRepository permissionRepository, Optional<CacheManager> cacheManager) {
        this.permissionRepository = permissionRepository;
        if(cacheManager.isPresent()) {
            String name = this.getClass().getCanonicalName();
            cache = CacheBuilder.newCache(cacheManager.get(), name, String.class, Permission.class)
                    .heap(100)
                    .offheap(100)
                    .build();
        }
    }

    public List<Permission> findAll(Predicate predicate) {
        return permissionRepository.findAll(predicate);
    }

    public List<Permission> findAll() {
        return this.findAll(Predicate.empty());
    }

    public Optional<Permission> find(String uuid) {
        Optional<Permission> permission = (cache == null) ? Optional.empty() : Optional.ofNullable(cache.get(uuid));
        if(!permission.isPresent()) {
            permission = permissionRepository.find(uuid);
            if(cache != null) {
                cache.put(uuid, permission.get());
            }
        }
        return permission;
    }

    public Permission get(String uuid) {
        return this.find(uuid).get();
    }


    public Optional<Permission> findById(String id) {
        Optional<Permission> permission = (cache == null) ? Optional.empty() : Optional.ofNullable(cache.get(id));
        if(!permission.isPresent()) {
            permission = permissionRepository.findById(id);
            if(cache != null) {
                cache.put(id, permission.get());
            }
        }
        return permission;
    }

    public Permission getById(String id) {
        return this.findById(id).get();
    }

    public List<Permission> findAllByIds(List<String> ids) {
        List<Permission> permissions = new ArrayList<>();
        for(String id : ids) {
            Permission permission = this.findById(id)
                    .orElseThrow(() -> new NotFoundException(String.format("Permission(%s) not found.", id)));
            permissions.add(permission);
        }
        return permissions;
    }

    @Transactional
    public Permission save(Permission permission) {
        return permissionRepository.save(permission);
    }

    public void handleBeforeAnyUpdate(Permission permission) {
        if(cache != null) {
            cache.remove((permission.getId()));
            cache.remove((permission.getUuid()));
        }
    }
}
