package io.leandev.appstack.web.service;

import io.leandev.appstack.web.config.WebSecurityConfiguration;
import io.leandev.appstack.web.entity.Privilege;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import java.util.Optional;

@Slf4j
public class PrivilegeListener {
    @PreUpdate
    @PreRemove
    private void beforeAnyUpdate(Privilege privilege) {
        Optional<PrivilegeService> privilegeService = WebSecurityConfiguration.getBean(PrivilegeService.class);
        if(privilegeService.isPresent()) {
            privilegeService.get().handleBeforeAnyUpdate(privilege);
        }
    }
}
