package io.leandev.appstack.web.service;

import io.leandev.appstack.cache.CacheBuilder;
import io.leandev.appstack.env.Environ;
import io.leandev.appstack.search.Predicate;
import io.leandev.appstack.web.entity.Privilege;
import io.leandev.appstack.web.repository.PrivilegeRepository;
import lombok.extern.slf4j.Slf4j;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class PrivilegeService {
    private static final Environ environ = Environ.getInstance();

    private final PrivilegeRepository privilegeRepository;
    // key: uuid or id
    private Cache<String, Privilege> cache = null;

    public PrivilegeService(PrivilegeRepository privilegeRepository, Optional<CacheManager> cacheManager) {
        this.privilegeRepository = privilegeRepository;
        if(cacheManager.isPresent()) {
            String name = this.getClass().getCanonicalName();
            cache = CacheBuilder.newCache(cacheManager.get(), name, String.class, Privilege.class)
                    .heap(100)
                    .offheap(100)
                    .build();
        }
    }

    public List<Privilege> findAll(Predicate predicate) {
        return privilegeRepository.findAll(predicate);
    }

    public List<Privilege> findAll() {
        return this.findAll(Predicate.empty());
    }
    public Optional<Privilege> find(String uuid) {
        Optional<Privilege> privilege = (cache == null) ? Optional.empty() : Optional.ofNullable(cache.get(uuid));
        if(!privilege.isPresent()) {
            privilege = privilegeRepository.find(uuid);
            if(cache != null) {
                cache.put(uuid, privilege.get());
            }
        }
        return privilege;
    }

    public Privilege get(String uuid) {
        return this.find(uuid).get();
    }
    public List<Privilege> findAllByIds(List<String> ids) {
        List<Privilege> privileges = new ArrayList<>();
        for(String id : ids) {
            Privilege privilege = this.findById(id)
                    .orElseThrow(() -> new NotFoundException(String.format("Privilege(%s) not found.", id)));
            privileges.add(privilege);
        }
        return privileges;
    }

    public Optional<Privilege> findById(String id) {
        Optional<Privilege> privilege = (cache == null) ? Optional.empty() : Optional.ofNullable(cache.get(id));
        if(!privilege.isPresent()) {
            privilege = privilegeRepository.findById(id);
            if(cache != null) {
                cache.put(id, privilege.get());
            }
        }
        return privilege;
    }

    public Privilege getById(String id) {
        return this.findById(id).get();
    }

    @Transactional
    public Privilege save(Privilege privilege) {
        return privilegeRepository.save(privilege);
    }

    public void handleBeforeAnyUpdate(Privilege privilege) {
        if(cache != null) {
            cache.remove((privilege.getUuid()));
            cache.remove((privilege.getId()));
        }
    }
}
