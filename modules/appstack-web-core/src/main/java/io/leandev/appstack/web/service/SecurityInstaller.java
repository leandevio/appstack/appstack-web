package io.leandev.appstack.web.service;

import io.leandev.appstack.web.entity.Pass;
import io.leandev.appstack.crypto.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SecurityInstaller {
    private final DataInstaller dataInstaller;
    private static final PasswordEncoder encoder = PasswordEncoder.getDefaultInstance();

    @Autowired
    public SecurityInstaller(DataInstaller dataInstaller) {
        this.dataInstaller = dataInstaller;
    }

    public void install() {
        dataInstaller.install(Pass.class, data -> {
            if(data.getActivationDate()==null) {
                data.setActivationDate(new Date());
            }
            String credentials = encoder.encode(data.getCredentials());
            data.setCredentials(credentials);
        });
    }
}
