package io.leandev.appstack.web.service;

import io.leandev.appstack.web.entity.Sequence;
import io.leandev.appstack.web.repository.SequenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PreDestroy;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantLock;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class SequenceService {
    private ConcurrentMap<String, Seed> seeds = new ConcurrentHashMap<>();
    private int BUFFER = 10;

    @Autowired
    SequenceRepository repository;

    @PreDestroy
    public void destroy() {
//        flush();
    }

    public long nextNumber(String code) {
        return nextNumber(code, 1);
    }

    public long nextNumber(String code, int increment) {
        Seed seed = getSeed(code);

        seed.lock();

        seed.current = seed.current + increment;

        seed.unlock();

        return seed.current;
    }

    public long currentNumber(String code) {
        return seeds.get(code).current;
    }

    public synchronized void flush() {
        for(String code : seeds.keySet()) {
            Seed seed = seeds.get(code);
            seed.max = seed.current;
            Sequence sequence = repository.findById(code).get();
            sequence.setSeed(seed.current);
            repository.save(sequence);
        }
    }

    private synchronized Seed getSeed(String code) {
        Seed seed = seeds.get(code);

        if(seed==null) {
            seed = new Seed();
            Optional<Sequence> bean = repository.findById(code);
            Sequence sequence;
            if(bean.isPresent()) {
                sequence = bean.get();
                seed.current = sequence.getSeed();
            } else {
                sequence = new Sequence();
                sequence.setCode(code);
            }
            long max = sequence.getSeed() + BUFFER;
            sequence.setSeed(max);
            repository.save(sequence);
            seed.max = max;
            seeds.put(sequence.getCode(), seed);
        } else if(seed.current>=seed.max) {
            Sequence sequence = repository.findById(code).get();
            long current = sequence.getSeed();
            long max = sequence.getSeed() + BUFFER;
            sequence.setSeed(max);
            repository.save(sequence);
            seed.current = current;
            seed.max = max;
        }

        return seed;
    }

    private class Seed {
        public long current = 0L;
        public long max = 0L;
        private ReentrantLock lock = new ReentrantLock();

        public void lock() {
            lock.lock();
        }

        public void unlock() {
            lock.unlock();
        }
    }
}
