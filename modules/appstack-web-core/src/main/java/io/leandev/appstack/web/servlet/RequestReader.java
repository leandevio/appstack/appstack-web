package io.leandev.appstack.web.servlet;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Slf4j
public class RequestReader {
    private static final String[] CLIENT_IP_PROXY_HEADERS = {"X-Forwarded-For", "Proxy-Client-IP", "WL- Proxy-Client-IP", "HTTP_CLIENT_IP", "X-Real-IP"};
    public Optional<InetAddress> resolveRemoteAddr(HttpServletRequest request) {
        InetAddress inetAddress = null;

        String remoteHost = request.getRemoteHost();
        String remoteAddr = request.getRemoteAddr();
        if(log.isDebugEnabled()) {
            log.debug(String.format("The remote host resolved from the request is: %s.", remoteHost));
            log.debug(String.format("The remote address resolved from the request is: %s.", remoteAddr));
        }
        try {
            if(remoteAddr != null) {
                inetAddress = Inet6Address.getByName(remoteAddr);
            } else if(remoteHost != null) {
                inetAddress = Inet6Address.getByName(remoteHost);
            }
        } catch (UnknownHostException e) {
            log.warn("Failed to identify the remote address");
        }
        return Optional.ofNullable(inetAddress);
    }

    public List<InetAddress> resolveAllClientAddr(HttpServletRequest request) {
        List<InetAddress> allClientAddr = new ArrayList<>();
        String ips = null;
        for(String header : CLIENT_IP_PROXY_HEADERS) {
            ips = request.getHeader(header);
            if (ips != null && ips.length() > 0 && !"unknown".equalsIgnoreCase(ips)) {
                break;
            }
        }
        if (ips == null || ips.length() == 0 || "unknown".equalsIgnoreCase(ips)) {
            Optional<InetAddress> remoteAddr = this.resolveRemoteAddr(request);
            if(remoteAddr.isPresent()) {
                allClientAddr.add(remoteAddr.get());
            }
        } else {
            for(String ip : ips.split(",")) {
                try {
                    InetAddress inetAddress = Inet6Address.getByName(ip);
                    allClientAddr.add(inetAddress);
                } catch (UnknownHostException e) {
                    log.warn("Failed to identify the remote address");
                }
            }
        }

        return allClientAddr;
    }

    public Optional<InetAddress> resolveClientAddr(HttpServletRequest request) {
        Optional<InetAddress> clientAddr = Optional.empty();
        String ips = null;
        for(String header : CLIENT_IP_PROXY_HEADERS) {
            ips = request.getHeader(header);
            if (ips != null && ips.length() > 0 && !"unknown".equalsIgnoreCase(ips)) {
                break;
            }
        }
        if (ips == null || ips.length() == 0 || "unknown".equalsIgnoreCase(ips)) {
            clientAddr = this.resolveRemoteAddr(request);
        } else {
            String ip = Arrays.stream(ips.split(",")).findFirst().get();
            try {
                InetAddress inetAddress = Inet6Address.getByName(ip);
                clientAddr = Optional.ofNullable(inetAddress);
            } catch (UnknownHostException e) {
                log.warn("Failed to identify the remote address");
            }
        }

        return clientAddr;
    }
}
