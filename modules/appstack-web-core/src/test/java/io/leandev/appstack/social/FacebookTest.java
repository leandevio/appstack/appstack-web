package io.leandev.appstack.social;

import io.leandev.appstack.security.exception.AuthenticationException;
import org.junit.jupiter.api.Test;

import java.awt.image.BufferedImage;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;

public class FacebookTest {
    private static final String accessToken = "EAAKoZAgjdAVABADIwuLTqXUKcV0Dl6OeSTFTISZAgEmNsOqUwEht5lmABSOQjAO1qwyRVSB327grTpDgpafGZAZCEo4tVLxkgIcWlYRQXo59v3ZAjPr0owxRyrvMOPUAt5M5M7NpI1DG1GgSq2kdMrco7rZCjBZC4KGSvEmazElCyAyPKmnOM3TuSIerOo9tIWVJz0xY7QmIQZDZD";
    private static final String expiredAccessToken = "EAAKoZAgjdAVABAL7GlNUpDzwbSXhuijYj7qapDIL3TwI5bWwIKcYtLk0bO8ZABQipPFohksYpSWsg470uDMkljVPZAkFse7BVZCPp7fl7O4cGVXYpKhGxFmdJwKlIiAL81o159iorZCJZC2ZBhLvhdWonDU78m88feV3jcHqC3ZCy2aGDCOawtuuvkMvcQdCszUHQ4hBiIM8sgZDZD";
    private static final String invalidAccessToken = "123456";
    private static final String EMAIL = "jasonlin@leandev.com.tw";

    @Test
    public void testConnect() throws Exception {
        Facebook facebook = Facebook.connect(accessToken);
        assertThat(facebook.email()).isEqualTo(EMAIL);
    }

    @Test
    public void testGetPicture() throws Exception {
        Facebook facebook = Facebook.connect(accessToken);
        BufferedImage picture = facebook.picture(200, 200);
        assertThat(picture.getWidth()).isEqualTo(200);
    }

    @Test
    public void testExpiredAccessToken() {
        assertThatExceptionOfType(AuthenticationException.class)
                .isThrownBy(() -> Facebook.connect(expiredAccessToken));
    }

    @Test
    public void testInvalidAccessToken() throws Exception {
        assertThatExceptionOfType(AuthenticationException.class)
                .isThrownBy(() -> Facebook.connect(invalidAccessToken));
    }
}