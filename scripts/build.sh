#!/bin/bash

# build - A script to build the application.

##### Constants
LIB="lib"
MODULES="modules"

WORKSPACE="workspace"
WORK=".work"
##### Functions

function usage
{
    echo "usage: build.sh [-module <module>]"
}

function log {
    local message=$1
    echo "${script}: ${message}"
}

function build
{
    local status=0

    log "Build application: ${modules}"

    for module in ${libs[@]}; do
        cd ${modules}/${module}
        log "package & install the module: ${module} => ${modules}/${module}/target/${module}.jar"
        mvn clean install -Dmaven.test.skip=true
        if [[ $? != 0 ]]; then
            status=1
            log "Failed to package & install the ${module}!"
            break
        fi
    done

    if [[ ${status} != 0 ]]; then
        return ${status}
    fi

    for module in ${webs[@]}; do
        cd ${modules}/${module}
        local pom="pom.xml"
        local warname=`detectWarName ${pom}`
        log "package the module: ${module} => ${modules}/${module}/target/${warname}.war"

        mvn clean package -Dmaven.test.skip=true
        if [[ $? != 0 ]]; then
            status=1
            log "Failed to package the ${module}!"
            break
        fi
    done

    return ${status}
}

function detect
{
    local status=0
    local jars=()
    local wars=()

    log "Detect modules: ${modules}"

    for path in ${modules}/*; do
        local pom=${path}/pom.xml
        if [[ ! -f ${pom} ]]; then
            continue
        fi
        local module=`basename ${path}`
        local archive=`detectArchive ${pom}`
        case ${archive} in
           "jar")
            jars=(${jars[@]} ${module})
            ;;
          "war")
            wars=(${wars[@]} ${module})
            ;;
          *)
            ;;
        esac
    done

    if [[ ${#libs[@]} == 0 ]]; then
        libs=(${jars[@]})
    fi

    if [[ ${#webs[@]} == 0 ]]; then
        webs=(${wars[@]})
    fi

    return ${status}
}

function detectArchive
{
    local pom=$1
    local archive=`grep "<packaging>[a-z]*</packaging>" ${pom} | sed "s/<packaging>//" | sed "s/<\/packaging>//" | sed "s/ //g"`
    echo ${archive}
}

function detectWarName
{
    local pom=$1
    local warname=`grep "<project.build.warname>[a-zA-Z-]*<\/project.build.warname>" ${pom} | sed "s/<project.build.warname>//" | sed "s/<\/project.build.warname>//" | sed "s/ //g"`
    echo ${warname}
}

##### Main
script=$0
status=0
workspace=$REPO_WORKSPACE
libs=(${APP_LIBS})
webs=(${APP_WEBS})
modules=${workspace}/${MODULES}

while [[ "$1" != "" ]]; do
    case $1 in
        -module )       shift
                        module=$1
                        ;;
        -h | --help )   usage
                        exit
                        ;;
        * )             usage
                        exit 1
    esac
    shift
done

# Verify environment variables

if [[ ${workspace} == '' ]]; then
    workspace=$HOME/${WORKSPACE}
    modules=${workspace}/${MODULES}
fi

# Verify command line processing

if [[ -n ${module} ]]; then
        webs=(${module})
fi

log "Building the application ......"

if [[ ${status} == 0 ]]; then
    detect
    if [[ $? != 0 ]]; then status=1; fi
fi

if [[ ${status} == 0 ]]; then
    build
    if [[ $? != 0 ]]; then status=1; fi
fi

if [[ ${status} == 0 ]]; then
    log "Job done.";
else
    log "Job failed!"
fi

exit ${status}
