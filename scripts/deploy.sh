#!/bin/bash

# deploy - A script to deploy the application.

##### Constants
LIB="lib"
MODULES="modules"

WORKSPACE="workspace"
WORK=".work"
##### Functions

function usage
{
    echo "usage: deploy.sh [-module module]"
}

function log {
    local message=$1
    echo "${script}: ${message}"
}

function deploy
{
    local status=0
    local webapps=${CATALINA_HOME}/webapps

    if [[ ! -d ${webapps} ]]; then
        log "Failed to deploy the module: ${webapps} is missing."
        return 1
    fi

    for module in ${webs[@]}; do
        local pom=${modules}/${module}/pom.xml;
        local warname=`detectWarName ${pom}`
        local war=${modules}/${module}/target/${warname}.war

        log "Deploy the module: ${module} ${war} => ${webapps}/${warname}.war"

        if [[ ! -f ${war} ]]; then
            status=1
            log "Failed to deploy the ${module}: ${war} is missing."
            break
        fi
        rm -fr ${webapps}/${warname}
        rm -f ${webapps}/${warname}.war

        cp ${war} ${webapps}
        if [[ $? != 0 ]]; then
            status=1
            log "Failed to deploy the ${module}: ${war} => ${webapps}/${warname}.war"
            break
        fi
    done

    return ${status}
}

function detect
{
    local status=0
    local wars=()

    log "Detect modules: ${modules}"

    for path in ${modules}/*; do
        local pom=${path}/pom.xml;
        if [[ ! -f ${pom} ]]; then
            continue
        fi
        local module=`basename ${path}`
        local archive=`detectArchive ${pom}`
        if [[ ${archive} != "war" ]]; then
            continue
        fi
        wars=(${wars[@]} ${module})
    done

    if [[ ${#webs[@]} == 0 ]]; then
        webs=(${wars[@]})
    fi

    return ${status}
}

function detectArchive
{
    local pom=$1
    local archive=`grep "<packaging>[a-z]*</packaging>" ${pom} | sed "s/<packaging>//" | sed "s/<\/packaging>//" | sed "s/ //g"`
    echo ${archive}
}

function detectWarName
{
    local pom=$1
    local warname=`grep "<project.build.warname>[a-zA-Z-]*<\/project.build.warname>" ${pom} | sed "s/<project.build.warname>//" | sed "s/<\/project.build.warname>//" | sed "s/ //g"`
    echo ${warname}
}

##### Main
script=$0
status=0
workspace=$REPO_WORKSPACE
webs=(${APP_WEBS})
modules=${workspace}/${MODULES}
module=

while [[ $1 != "" ]]; do
    case $1 in
        -module )    shift
                        module=$1
                        ;;
        -h | --help )   usage
                        exit
                        ;;
        * )             usage
                        exit 1
    esac
    shift
done

# Verify environment variables

if [[ ${workspace} == '' ]]; then
    workspace=$HOME/${WORKSPACE}
    modules=${workspace}/${MODULES}
fi


# Verify command line processing

if [[ -n ${module} ]]; then
        webs=(${module})
fi

log "Deploying the application ......"

if [[ ${status} == 0 ]]; then
    detect
    if [[ $? != 0 ]]; then status=1; fi
fi

if [[ ${status} == 0 ]]; then
    deploy
    if [[ $? != 0 ]]; then status=1; fi
fi

if [[ ${status} == 0 ]]; then
    log "Job done.";
else
    log "Job failed!"
fi

exit ${status}
