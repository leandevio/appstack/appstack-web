#!/bin/bash

# prepare - A script to prepare the environment for building & deploying the artifacts.

##### Constants
CONF="conf"
LIB="lib"
MODULES="modules"

WORKSPACE="workspace"
WORK=".work"
##### Functions

function usage {
    echo "usage: prepare.sh [-profile profile]"
}

function log {
    local message=$1
    echo "${script}: ${message}"
}

function setup {
    mkdir -p ${workspace}
    mkdir -p ${work}
}

function prepare {
    local status=0
    local tarball=${work}/libs.tar.gz
    local lib=${work}/lib

    rm -fr ${lib}

    if [[ -z ${profile} ]]; then
        return 0
    fi

    git archive --format=tar.gz -o ${tarball} HEAD --remote=${url} ${LIB}/${profile}
    # return code = 1 means the specified remote path does not exist.
    status=$?
    if [[ ${status} != 0 ]]; then
        if [[ ${status} == 1 ]]; then
            return 0
        else
            log "Failed to download the library: ${url}/lib => ${tarball}"
            return 1
        fi
    fi

    tar -zxvf ${tarball} -C ${work}
    if [[ $? != 0 ]]; then
         log "Failed to extract the downloaded tarball: ${tarball} => ${work}"
             return 1
    fi

    for path in ${lib}/${profile}/*.jar; do
        local artifact=`basename ${path} .jar`
        log "Install the ${path} => ${group}.${artifact}"
        mvn install:install-file -Dfile=${path} -Dpackaging=jar -DgroupId=${group} -DartifactId=${artifact} -Dversion=1.0.0
        if [[ $? != 0 ]]; then
            status=1
            log "Failed to install the ${path} => ${group}.${artifact}"
            break
        fi
    done

    rm -f ${tarball}
    rm -fr ${lib}

    return ${status}
}

##### Main
script=$0
status=0
url=${REPO_URL}
workspace=$REPO_WORKSPACE
profile=${LIB_PROFILE}
group=${LIB_GROUP}
work=$HOME/${WORK}

while [[ "$1" != "" ]]; do
    case $1 in
        -profile )    shift
                    profile=$1
                    ;;
        -h | --help )   usage
                        exit
                        ;;
        * )             usage
                        exit 1
    esac
    shift
done

# Verify environment variables

if [[ ${workspace} == '' ]]; then
    workspace=$HOME/${WORKSPACE}
fi


# Verify command line processing

log "Preparing the environment ......"

if [[ ${status} == 0 ]]; then
    setup
    if [[ $? != 0 ]]; then status=1; fi
fi

if [[ ${status} == 0 ]]; then
    prepare
    if [[ $? != 0 ]]; then status=1; fi
fi

if [[ ${status} == 0 ]]; then
    log "Job done.";
else
    log "Job failed!"
fi

exit ${status}
