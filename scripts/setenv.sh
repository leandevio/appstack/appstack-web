#!/bin/bash

# Application settings
export APP_LIBS="<lib/jar modules>"
export APP_WEBS="<web/war modules>"

# Repository settings
export REPO_URL="<Git Repository URL>"
export REPO_REVISION="HEAD"
export REPO_WORKSPACE=$HOME/workspace
#export REPO_LOCKS=

# Library settings
#export LIB_PROFILE="<Library folder>"
#export LIB_GROUP="<Maven Group ID for Library>"

# Java settings
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export JAVA_OPTS="-server -Djava.awt.headless=true -Xms128m -Xmx1024m -XX:PermSize=128m -XX:MaxPermSize=1024m -Dfile.encoding=UTF8"
export PATH=${JAVA_HOME}/bin:$PATH

# Tomcat settings
export CATALINA_HOME=$HOME/opt/apache-tomcat-8.5.46

# Maven settings
export M2_HOME=$HOME/opt/apache-maven-3.6.2
export M2=${M2_HOME}/bin
export MAVEN_OPTS="-Xmx1024m -XX:MaxPermSize=256m"
export PATH=$HOME/bin:${M2}:$PATH
