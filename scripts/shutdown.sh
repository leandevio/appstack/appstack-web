#!/bin/bash

# shutdown - A script to shutdown the application.

##### Constants
LIB="lib"
MODULES="modules"

WORKSPACE="workspace"
WORK=".work"
##### Functions

function usage
{
    echo "usage: shutdown.sh"
}

function log {
    local message=$1
    echo "${script}: ${message}"
}

function shutdown
{
    ${CATALINA_HOME}/bin/shutdown.sh
    if [[ $? != 0 ]]; then return 1; fi

    return 0
}

##### Main
script=$0
status=0

while [[ "$1" != "" ]]; do
    case $1 in
        -h | --help )   usage
                        exit
                        ;;
        * )             usage
                        exit 1
    esac
    shift
done

# Verify command line processing

log "Shutdown the application ......"

if [[ ${status} == 0 ]]; then
    shutdown
    if [[ $? != 0 ]]; then status=1; fi
fi

if [[ ${status} == 0 ]]; then
    log "Job done.";
else
    log "Job failed!"
fi

exit ${status}
