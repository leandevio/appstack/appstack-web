#!/bin/bash
# startup - A script to startup the application.

##### Constants
LIB="lib"
MODULES="modules"

WORKSPACE="workspace"
WORK=".work"
##### Functions

function usage
{
    echo "usage: startup.sh"
}

function log {
    local message=$1
    echo "${script}: ${message}"
}

function startup
{
    ${CATALINA_HOME}/bin/startup.sh
    if [[ $? != 0 ]]; then return 1; fi

    return 0
}

##### Main
script=$0
status=0

while [[ $1 != "" ]]; do
    case $1 in
        -h | --help )   usage
                        exit
                        ;;
        * )             usage
                        exit 1
    esac
    shift
done

# Verify command line processing

log "Startup the application ......"

if [[ ${status} == 0 ]]; then
    startup
    if [[ $? != 0 ]]; then status=1; fi
fi

if [[ ${status} == 0 ]]; then
    log "Job done.";
else
    log "Job failed!"
fi

exit ${status}
