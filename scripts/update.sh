#!/bin/bash
# update - A script to update the source code.o

##### Constants
REVISION="HEAD"
LIB="lib"
MODULES="modules"

WORKSPACE="workspace"
WORK=".work"
##### Functions

function usage
{
    echo "usage: update.sh [-lock <lock file>] [-revision <revision>]"
}

function log {
    local message=$1
    echo "${script}: ${message}"
}

function update {
    local status=0
    local tarball="${work}/latest.tar.gz"

    log "Update the source code: ${url} (${revision}) => ${workspace}"

    git archive --format=tar.gz -o ${tarball} ${revision} --remote=${url} modules
    if [[ $? != 0 ]]; then
         log "Failed to download the source code: ${url}/modules (${revision}) => $tarball"
         return 1
    fi

    tar -zxvf "$tarball" -C "$workspace"
    if [[ $? != 0 ]]; then
         log "Failed to extract the downloaded tarball: $tarball => $workspace"
         status=1
    fi

    rm -f "$tarball"

    return ${status}
}

function backup {
    local status=0

    if [[ -z "$(ls -A ${workspace})" ]]; then
        return 0
    fi

    if [[ -d ${bak} ]]; then
        log "A previous backup already exists: $bak"
        return 1
    fi

    log "Backup the files: $workspace => $bak"

    mkdir -p ${bak}

    mv ${workspace}/* ${bak}/

    return ${status}
}

function restore {
    local status=0
    local currdir=`pwd`

    if [[ ! -d ${bak} ]]; then
        return 0
    fi

    log "Restore the specified files: $bak => $workspace"

    cd ${bak}
    for lock in ${locks[@]}; do
        find . -name ${lock} | while read file; do
            local folder=`dirname "${file}"`
            mkdir -p "${workspace}"/"${folder}"
            local path=`basename ${file}`
            log "Restore ${workspace}/${folder}/${path}"
            cp "${file}" "${workspace}"/"${folder}"
            if [[ $? != 0 ]]; then
                 log "Failed to restore the configuration file - $file!"
                 status=1
                 break
            fi
        done
    done

    cd ${currdir}

    if [[ ${status} == 0 ]]; then
        rm -fr ${bak}
    fi

    return ${status}
}

function rollback {
    local status=0

    if [[ ! -d ${bak} ]]; then
        return 0
    fi

    log "Restore the files: $bak => $workspace"

    mv ${bak}/* ${workspace}
    if [[ $? != 0 ]]; then
         log "Failed to extract the downloaded tarball: $tarball => $workspace"
         return 1
    fi

    rm -fr ${bak}

    return ${status}
}

##### Main
script=$0
status=0
url=$REPO_URL
workspace=$REPO_WORKSPACE
work=$HOME/${WORK}
bak="${work}/bak"
modules=${workspace}/${MODULES}
locks=(${REPO_LOCKS})
revision=$REPO_REVISION

while [[ "$1" != "" ]]; do
    case $1 in
        -lock )       shift
                        lock=$1
                        ;;
        -revision )     shift
                        revision=$1
                        ;;
        -h | --help )   usage
                        exit
                        ;;
        * )             usage
                        exit 1
    esac
    shift
done

# Verify environment variables

if [[ ${workspace} == '' ]]; then
    workspace=$HOME/${WORKSPACE}
    modules=${workspace}/${MODULES}
fi

if [[ ${revision} == '' ]]; then
    revision=${REVISION}
fi

# Verify command line processing

if [[ -n ${lock} ]]; then
        locks=(${lock})
fi

if [[ -n ${revision} ]]; then
        revision=(${revision})
fi

log "Updating the application source code ......"

if [[ ${status} == 0 ]]; then
    backup
    if [[ $? != 0 ]]; then status=1; fi
fi

if [[ ${status} == 0 ]]; then
    update
    if [[ $? != 0 ]]; then status=1; fi
fi

if [[ ${status} == 0 ]]; then
    restore
    if [[ $? != 0 ]]; then status=1; fi
fi

if [[ ${status} != 0 ]]; then
    rollback
fi

if [[ ${status} == 0 ]]; then
    log "Job done.";
else
    log "Job failed!"
fi

exit ${status}
